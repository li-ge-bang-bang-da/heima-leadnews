package com.heima.admin.listener;

import com.alibaba.fastjson.JSON;
import com.heima.admin.service.WemediaNewsAutoScanService;
import com.heima.model.common.constants.NewsAutoScanConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
@Slf4j
public class WemediaNewsAutoListener {

    @Autowired
    WemediaNewsAutoScanService wemediaNewsAutoScanService;

    @KafkaListener(topics = NewsAutoScanConstants.WM_NEWS_AUTO_SCAN_TOPIC)
    public void recevieMessage(String message) {
        log.info("WemediaNewsAutoListener recevieMessage :{}", message);

        if (StringUtils.isNotBlank(message)) {
            Map<String,Integer> map = JSON.parseObject(message, Map.class);
            wemediaNewsAutoScanService.autoScanByMediaNewsId(map.get("wmNewsId"));
        }

        log.info("*******************自媒体文章审核成功*******************");
    }
}