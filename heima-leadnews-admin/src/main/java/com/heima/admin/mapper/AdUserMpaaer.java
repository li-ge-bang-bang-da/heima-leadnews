package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojos.AdUser;

public interface AdUserMpaaer extends BaseMapper<AdUser> {
}
