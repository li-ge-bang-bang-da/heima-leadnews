package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdChannelMapper;
import com.heima.admin.service.AdChannelService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Service
@Transactional
public class AdChannelServiceImpl extends ServiceImpl<AdChannelMapper, AdChannel> implements AdChannelService {

    /**
     * 修改
     *
     * @param adChannel
     * @return
     */
    @Override
    public ResponseResult update(AdChannel adChannel) {
        //1.校验参数
        if (adChannel ==null || StringUtils.isBlank(adChannel.getName())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (adChannel.getId()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.根据ID先查后改
        AdChannel id = getById(adChannel.getId());
        if (id == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        if (adChannel.getIsDefault() != null ){ //是否传参
            if (adChannel.getIsDefault()){
                LambdaQueryWrapper<AdChannel> queryWrapper = new LambdaQueryWrapper<>();
                queryWrapper.eq(AdChannel::getIsDefault,1);
                AdChannel one = getOne(queryWrapper);
                if (one != null){
                    one.setIsDefault(false);
                    updateById(one);
                }
            }
        }



        updateById(adChannel);
        //3.返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult deleteById(Integer id) {
        //1.校验参数
        if (id == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.根据ID先查后删
        AdChannel channel = getById(id);
        if (channel == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        if (channel.getIsDefault()){
            return ResponseResult.errorResult(AppHttpCodeEnum.CHANNEL_ISDEFAULT_INVALID);
        }
        if (channel.getStatus()){
            return ResponseResult.errorResult(AppHttpCodeEnum.ADMIN_CHANNEL_ISSTATUS_ERROR);
        }

        //TODO:
//        int i = 1/0;

        removeById(id);
        //3.返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 新增
     *
     * @param adChannel
     * @return
     */
    @Override
    public ResponseResult insert(AdChannel adChannel) {
        //1.校验参数
        if (adChannel == null || StringUtils.isBlank(adChannel.getName())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.执行新增
        adChannel.setCreatedTime(new Date());

        if (adChannel.getIsDefault() != null ){
            if (adChannel.getIsDefault()){
                LambdaQueryWrapper<AdChannel> queryWrapper = new LambdaQueryWrapper<>();
                queryWrapper.eq(AdChannel::getIsDefault,1);
                AdChannel one = getOne(queryWrapper);
                if (one != null){
                    one.setIsDefault(false);
                    updateById(one);
                }
            }
        }

//        if (true){
//            CustException.cust(AppHttpCodeEnum.NEED_ADMIND);
//        }

        save(adChannel);
        //3.返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 查询频道列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findByNameAndPage(ChannelDto dto) {
        //1.校验参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();
        //2.按名称分页查询
        IPage<AdChannel> page = new Page<>(dto.getPage(), dto.getSize());
        LambdaQueryWrapper<AdChannel> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        //频道模糊查询
        if (StringUtils.isNoneBlank(dto.getName())) {
            lambdaQueryWrapper.like(AdChannel::getName, dto.getName());
        }
        //状态查询
        if (dto.getStatus() != null) {
            lambdaQueryWrapper.like(AdChannel::getStatus, dto.getStatus());
        }
        IPage<AdChannel> result = page(page, lambdaQueryWrapper);

        //3.封装结果返回
        PageResponseResult responseResult = new PageResponseResult(dto.getPage(), dto.getSize(),
                                                    result.getTotal(), result.getRecords());

        return responseResult;
    }

}
