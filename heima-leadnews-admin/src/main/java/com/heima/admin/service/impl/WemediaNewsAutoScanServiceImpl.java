package com.heima.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.admin.mapper.AdChannelMapper;
import com.heima.admin.mapper.AdSensitiveMapper;
import com.heima.admin.service.WemediaNewsAutoScanService;
import com.heima.aliyun.GreeTextScan;
import com.heima.aliyun.GreenImageScan;
import com.heima.common.exception.CustException;
import com.heima.feigns.article.ArticleFeign;
import com.heima.feigns.wemedia.WemediaFeign;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.constants.ArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.common.SensitiveWordUtil;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class WemediaNewsAutoScanServiceImpl implements WemediaNewsAutoScanService {

    @Autowired
    WemediaFeign wemediaFeign;

    @Value("${file.oss.web-site}")
    private String webSite;

    @Autowired
    AdSensitiveMapper adSensitiveMapper;

    @Autowired
    GreeTextScan greeTextScan;

    @Autowired
    GreenImageScan greenImageScan;

    @Autowired
    AdChannelMapper adChannelMapper;

    @Autowired
    ArticleFeign articleFeign;

    @Autowired
    KafkaTemplate kafkaTemplate;

    /**
     * 自媒体文章审核
     *
     * @param wmNewsId
     */
    @Override
    @GlobalTransactional(rollbackFor = Exception.class)
    public void autoScanByMediaNewsId(Integer wmNewsId) {
        //参数校验
        if (wmNewsId == null) {
            log.error("当前id为空");
            CustException.cust(AppHttpCodeEnum.PARAM_INVALID);
            return;
        }
        //根据文章的id查询自媒体文章
        WmNews wmNews = wemediaFeign.findById(wmNewsId);
        if (wmNews == null) {
            log.error("未查到自媒体文章，自媒体id：{}" + wmNewsId);
            CustException.cust(AppHttpCodeEnum.DATA_NOT_EXIST);
            return;
        }

        //文章状态=4（人工审核通过） 且发布时间小于等于当前时间，直接保存文章库
        if (wmNews.getStatus().equals(WmNews.Status.ADMIN_SUCCESS.getCode()) &&
                wmNews.getPublishTime().getTime() < System.currentTimeMillis()) {
            saveAppArticle(wmNews);
            return;
        }

        //文章状态=8（人工审核通过，未发布） 且发布时间小于等于当前时间，直接保存文章库
        if (wmNews.getStatus().equals(WmNews.Status.SUCCESS.getCode()) &&
                wmNews.getPublishTime().getTime() < System.currentTimeMillis()) {
            saveAppArticle(wmNews);
            return;
        }

        //文章状态=1 未审核
        if (wmNews.getStatus().equals(WmNews.Status.SUBMIT.getCode())) {
            //抽取文章的内容和图片
            Map<String, Object> contentAndImagesResult = handleTextAndImages(wmNews);

            //自管理敏感词(审核成功true，审核失败返回false且修改文章的状态)
            boolean isSensitiveScan = handleSensitive((String) contentAndImagesResult.get("content"), wmNews);
            if (!isSensitiveScan) {
                return;
            }
            //阿里云文本内容审核(审核成功true，审核失败返回false且修改文章的状态)
            boolean isTextScan = handleTextScan((String) contentAndImagesResult.get("content"), wmNews);
            if (!isTextScan) {
                return;
            }

            //阿里云图片审核(审核成功true，审核失败返回false且修改文章的状态)
            boolean isImageScan = handleImagesScan((List) contentAndImagesResult.get("images"), wmNews);
            if (!isImageScan) {
                return;
            }

            //如果发布时间大于当前时间，修改文章状态=8
            if (wmNews.getPublishTime().getTime() > System.currentTimeMillis()) {
                updateWmNews(wmNews, WmNews.Status.SUCCESS.getCode(), "文章审核通过，待发布");
                return;
            }
        }


        //审核通过，保存文章相关信息（三张表） 修改文章的状态=9 已发布
        saveAppArticle(wmNews);

    }

    /**
     * 阿里云图片审核
     *
     * @param images
     * @param wmNews
     * @return
     */
    private boolean handleImagesScan(List images, WmNews wmNews) {
        boolean flag = true;

        try {
            Map map = greenImageScan.imageUrlScan(images);

            //审核不通过
            if (map.get("suggestion").equals("block")) {
                flag = false;
                updateWmNews(wmNews, WmNews.Status.FAIL.getCode(), "文章图片有违规");
            }
            //人工审核
            if (map.get("suggestion").equals("review")) {
                flag = false;
                updateWmNews(wmNews, WmNews.Status.ADMIN_AUTH.getCode(), "文章图片有不确定元素,需要人工审核");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("阿里云审核失败，转入人工审核, e:{}", e);
            flag = false;
            updateWmNews(wmNews, WmNews.Status.ADMIN_AUTH.getCode(), "阿里云审核失败，转入人工审核");
        }

        return flag;
    }

    /**
     * 阿里云文本内容审核
     *
     * @param content
     * @param wmNews
     * @return
     */
    private boolean handleTextScan(String content, WmNews wmNews) {
        boolean flag = true;

        try {
            Map map = greeTextScan.greeTextScan(content);

            //审核不通过
            if (map.get("suggestion").equals("block")) {
                flag = false;
                updateWmNews(wmNews, WmNews.Status.FAIL.getCode(), "文本中内容有敏感词汇");
            }

            //需要人工审核
            if (map.get("suggestion").equals("review")) {
                flag = false;
                updateWmNews(wmNews, WmNews.Status.ADMIN_AUTH.getCode(), "文章需要人工审核");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("文章阿里云内容审核失败,转入需要人工审核, e:{}", e);
            flag = false;
            updateWmNews(wmNews, WmNews.Status.ADMIN_AUTH.getCode(), "文章阿里云内容审核失败,转入人工审核");
        }
        return flag;
    }

    /**
     * 自管理敏感词审核
     *
     * @param content
     * @param wmNews
     * @return
     */
    private boolean handleSensitive(String content, WmNews wmNews) {
        boolean flag = true;
        // 查询所有敏感词
        List<String> sensitiveList = adSensitiveMapper.findAllSensitive();

        // 初始化 DFA搜索
        SensitiveWordUtil.initMap(sensitiveList);

        // 匹配敏感词
        Map<String, Integer> map = SensitiveWordUtil.matchWords(content);
        if (map.size() > 0) { // >0表示有敏感词
            flag = false;
            updateWmNews(wmNews, WmNews.Status.FAIL.getCode(), "文章包含敏感词汇" + map);
        }

        return flag;
    }

    /**
     * 修改文章状态
     *
     * @param wmNews
     * @param code
     * @param reason
     */
    private void updateWmNews(WmNews wmNews, short code, String reason) {
        wmNews.setStatus(code);
        wmNews.setReason(reason);
        ResponseResult result = wemediaFeign.updateWmNews(wmNews);
        if (!result.getCode().equals(0)) {   //失败
            CustException.cust(AppHttpCodeEnum.WEMEDIA_UPDATE_ERROR);
        }
    }

    /**
     * 抽取文章的内容和图片
     *
     * @param wmNews
     * @return
     */
    private Map<String, Object> handleTextAndImages(WmNews wmNews) {
        //1 获取文章内容并转换对象
        String content = wmNews.getContent();
        if (content == null || content.length() > 10000) {
            return null;
        }
        List<Map> maps = JSON.parseArray(content, Map.class);

        //2 解析内容文本 Stream 流优化 --> String
        String contents = maps.stream().filter(x -> x.get("type").equals("text")).map(y -> y.get("value").toString())
                .collect(Collectors.joining("_hmtt_"));
        //文章标题参与审核
        contents = wmNews.getTitle() + "_hmtt_" + contents;

        //3 解析内容图片
        List<String> images = maps.stream().filter(x -> x.get("type").equals("image")).map(y -> y.get("value").toString())
                .collect(Collectors.toList());

        //4 处理封面前缀 1.jpg 2.jpg
        String image = wmNews.getImages();
        if (StringUtils.isNotBlank(image)) {
            images.addAll(Stream.of(image.split(",")).map(x -> webSite + x).collect(Collectors.toList()));
        }

        //5 封装结果
        Map<String, Object> result = new HashMap<>();
        result.put("content", contents);
        result.put("images", images);

        return result;
    }

    /**
     * 数据保存文章库
     *
     * @param wmNews
     */
    private void saveAppArticle(WmNews wmNews) {

        //保存成功  文章ID

        ResponseResult result = saveArticle(wmNews);
        if (!result.getCode().equals(0)) {
            throw new RuntimeException("文章审核保存app文章失败");
        }
        //设置文章ID
        wmNews.setArticleId((Long) result.getData());
        //保存自媒体信息
        updateWmNews(wmNews, (short) 9, "已发布");


    }

    /**
     * 执行保存文章（三张表）
     *
     * @param wmNews
     * @return
     */
    private ResponseResult saveArticle(WmNews wmNews) {

        //封装数据
        ArticleDto articleDto = new ArticleDto();
        BeanUtils.copyProperties(wmNews, articleDto);
        articleDto.setId(null);
        if (wmNews.getArticleId() != null) {
            articleDto.setId(wmNews.getArticleId());
        }
        //布局
        articleDto.setLayout(wmNews.getType());
        articleDto.setFlag((byte) 0);
        //设置频道
        if (wmNews.getChannelId() != null) {
            articleDto.setChannelId(wmNews.getChannelId());
            AdChannel channel = adChannelMapper.selectById(wmNews.getChannelId());
            if (channel != null){
                articleDto.setChannelName(channel.getName());
            }
        }

        //设置作者信息
        //查wm user状态
        WmUser wmUser = wemediaFeign.findWmUserById(wmNews.getUserId());
        if (wmUser == null || wmUser.getStatus().byteValue() !=9){      //非法用户
            CustException.cust(AppHttpCodeEnum.ADMIN_ADUSER_ISNTOEXIT_ERROR);
        }
        if (StringUtils.isNotBlank(wmUser.getName())){
            //自媒体用户名称不可变动
            articleDto.setAuthorName(wmUser.getName());
        }

        //文章内容
        articleDto.setContent(wmNews.getContent());

        //发起远程调用 保存 三张表数据, 返回值必须携带文章id
        return articleFeign.saveArticle(articleDto);
    }
}
