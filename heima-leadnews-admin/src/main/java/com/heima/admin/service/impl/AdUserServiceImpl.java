package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdUserMpaaer;
import com.heima.admin.service.AdUserService;
import com.heima.common.exception.CustException;
import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.constants.AdminConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.utils.common.AppJwtUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.HashMap;

@Service
@Transactional
public class AdUserServiceImpl extends ServiceImpl<AdUserMpaaer, AdUser> implements AdUserService {
    /**
     * 管理员用户登录
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult login(AdUserDto dto) {
        //校验参数
        if (dto == null || StringUtils.isBlank(dto.getName()) ||StringUtils.isBlank(dto.getPassword())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //根据用户名查询用户信息，获取盐
        AdUser adUser = getOne(Wrappers.<AdUser>lambdaQuery()
                .eq(AdUser::getName, dto.getName()).eq(AdUser::getStatus, AdminConstants.ADUSER_NORMAL_STATUS));

        if (adUser == null){
            CustException.cust(AppHttpCodeEnum.ADMIN_ADUSER_ISNTOEXIT_ERROR);
        }
        //盐
        String salt = adUser.getSalt();
        //数据库加密后的密码
        String dbpwd = adUser.getPassword();


        //按照相同的加密方式加密，比对密码
        String npwd = DigestUtils.md5DigestAsHex((dto.getPassword() + salt).getBytes());
        if (!npwd.equals(dbpwd)){
            CustException.cust(AppHttpCodeEnum.ADMIN_ADUSER_NAMEORPWD_ERROR);
        }

        //更新登录时间
        adUser.setLoginTime(new Date());
        updateById(adUser);

        //生成token和用户信息返回给前端
        String token = AppJwtUtil.getToken(adUser.getId().longValue());

        HashMap<String, Object> result = new HashMap<>();
        result.put("token",token);
        adUser.setPassword(null);
        adUser.setSalt(null);
        result.put("user",adUser);

        return ResponseResult.okResult(result);
    }

    /**
     * 管理员用户注册
     *
     * @param adUser
     * @return
     */
    @Override
    public ResponseResult register(AdUser adUser) {
        //校验参数
        if (StringUtils.isEmpty(adUser.getName())||StringUtils.isEmpty(adUser.getPassword())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        // 用户的密码加密规则是：MD5（密码 + salt）
        String salt = RandomStringUtils.randomAlphabetic(10);//获取随机10位数的盐
        System.out.println(salt);
        String password = adUser.getPassword();

        adUser.setPassword(DigestUtils.md5DigestAsHex((password + salt).getBytes()));
        adUser.setSalt(salt);
        adUser.setCreatedTime(new Date());
        adUser.setStatus(9);

        save(adUser);

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
