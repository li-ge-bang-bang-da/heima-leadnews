package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;

public interface AdUserService extends IService<AdUser> {

    /**
     * 管理员用户登录
     * @param dto
     * @return
     */
    public ResponseResult login(AdUserDto dto);

    /**
     * 管理员用户注册
     * @param adUser
     * @return
     */

    public ResponseResult register(AdUser adUser);
}
