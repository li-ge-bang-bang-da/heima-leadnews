package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdSensitiveMapper;
import com.heima.admin.service.AdSensitiveService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
public class AdSensitiveServiceImpl extends ServiceImpl<AdSensitiveMapper, AdSensitive> implements AdSensitiveService {
    /**
     * 查询敏感词列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findByNameAndPage(SensitiveDto dto) {
        //1.校验参数
        if (dto == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();

        //2.根据名称和分页查询
        IPage<AdSensitive> page = new Page<>(dto.getPage(),dto.getSize());
        LambdaQueryWrapper<AdSensitive> query = new LambdaQueryWrapper<>();
        //敏感词模糊查询
        if (StringUtils.isNoneBlank(dto.getName())){
            query.like(AdSensitive::getSensitives,dto.getName());
        }
        IPage<AdSensitive> result = page(page, query);


        //3.封装结果返回
        PageResponseResult responseResult = new PageResponseResult(dto.getPage(),
                                                    dto.getSize(), result.getTotal(), result.getRecords());


        return responseResult;
    }

    /**
     * 新增
     *
     * @param adSensitive
     * @return
     */
    @Override
    public ResponseResult insert(AdSensitive adSensitive) {
        //1.校验参数
        if (adSensitive == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.执行新增
        adSensitive.setCreatedTime(new Date());

        save(adSensitive);

        //3.返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 修改
     *
     * @param adSensitive
     * @return
     */
    @Override
    public ResponseResult update(AdSensitive adSensitive) {
        //1.校验参数
        if (adSensitive == null||adSensitive.getId()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.执行修改
        updateById(adSensitive);


        //3.返回结果

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult deleteById(Integer id) {
        //1.检查参数
        if (id ==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.根据ID先查后删
        AdSensitive one = getById(id);
        if (one == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        removeById(id);

        //3.返回结果

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
