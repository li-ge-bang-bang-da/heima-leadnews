package com.heima.admin.job;

import com.heima.admin.service.WemediaNewsAutoScanService;
import com.heima.feigns.wemedia.WemediaFeign;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class WeMediaNewsAutoScanJob {

    @Autowired
    WemediaFeign wemediaFeign;

    @Autowired
    WemediaNewsAutoScanService wemediaNewsAutoScanService;


    @XxlJob("wemediaAutoScanJob")
    public ReturnT<String> autoScanJob(String param){
        log.info("*********自媒体文章审核调度任务开始执行*********");

        List<Integer> idList = wemediaFeign.findRelease();
        if (idList != null && idList.size()>0){
            for (Integer id : idList) {
                wemediaNewsAutoScanService.autoScanByMediaNewsId(id);
            }
            log.info("*********自媒体文章审核调度任务结束执行*********");
        }

        return ReturnT.SUCCESS;
    }
}
