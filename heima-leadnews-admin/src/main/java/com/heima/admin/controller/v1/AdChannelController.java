package com.heima.admin.controller.v1;

import com.heima.admin.service.AdChannelService;
import com.heima.apis.admin.AdChannelControllerApi;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/channel")
@Api(value = "频道管理",tags = "频道管理-channel",description = "频道管理API")
public class AdChannelController implements AdChannelControllerApi {

    @Autowired
    AdChannelService adChannelService;

    /**
     * 新增
     *
     * @param adchannel
     * @return
     */
    @Override
    @PostMapping("save")
    @ApiOperation("新增频道")
    public ResponseResult insert(@RequestBody AdChannel adchannel) {
        return adChannelService.insert(adchannel);
    }

    /**
     * 修改
     *
     * @param adChannel
     * @return
     */
    @Override
    @PutMapping("update")
    @ApiOperation("修改频道")
    public ResponseResult update(@RequestBody AdChannel adChannel) {
        return adChannelService.update(adChannel);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    @DeleteMapping("/del/{id}")
    @ApiOperation("删除频道")
    public ResponseResult deleteById(@PathVariable("id") Integer id) {
        return adChannelService.deleteById(id);
    }

    /**
     * 查询所有频道
     *
     * @return
     */
    @Override
    @GetMapping("/channels")
    public ResponseResult findAll() {
        List<AdChannel> channelList = adChannelService.list();
        return ResponseResult.okResult(channelList);
    }

    /**
     * 查询频道列表
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/list")
    @ApiOperation("频道分页列表查询")
    public ResponseResult findByNameAndPage(@RequestBody ChannelDto dto) {
        ResponseResult byNameAndPage = adChannelService.findByNameAndPage(dto);
        return byNameAndPage;
    }

}
