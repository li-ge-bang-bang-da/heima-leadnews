package com.heima.admin.controller.v1;

import com.heima.admin.service.AdUserService;
import com.heima.apis.admin.LoginControllerApi;
import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController implements LoginControllerApi {

    @Autowired
    AdUserService adUserService;

    /**
     * 管理员用户登录
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/in")
    public ResponseResult login(@RequestBody AdUserDto dto) {
        return adUserService.login(dto);
    }

    /**
     * 管理员用户注册
     *
     * @param adUser
     * @return
     */
    @Override
    @PostMapping("register")
    public ResponseResult register(@RequestBody AdUser adUser) {
        return adUserService.register(adUser);
    }
}
