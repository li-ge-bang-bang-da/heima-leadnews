package com.heima.admin.controller.v1;

import com.heima.admin.service.AdSensitiveService;
import com.heima.apis.admin.AdSensitiveControllerApi;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/sensitive")
public class AdSensitiveController implements AdSensitiveControllerApi {

    @Autowired
    AdSensitiveService adSensitiveService;

    /**
     * 查询敏感词列表
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/list")
    public ResponseResult findByNameAndPage(@RequestBody SensitiveDto dto) {
        return adSensitiveService.findByNameAndPage(dto);
    }

    /**
     * 新增
     *
     * @param adSensitive
     * @return
     */
    @Override
    @PostMapping("/save")
    public ResponseResult insert(@RequestBody AdSensitive adSensitive) {
        return adSensitiveService.insert(adSensitive);
    }

    /**
     * 修改
     *
     * @param adSensitive
     * @return
     */
    @Override
    @PostMapping("/update")
    public ResponseResult update(@RequestBody AdSensitive adSensitive) {
        return adSensitiveService.update(adSensitive);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    @DeleteMapping("/del/{id}")
    public ResponseResult deleteById(@PathVariable("id") Integer id) {
        return adSensitiveService.deleteById(id);
    }
}
