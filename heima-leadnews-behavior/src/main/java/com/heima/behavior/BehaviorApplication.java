package com.heima.behavior;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

/**
 * @Description:
 * @Version: V1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.heima.behavior.mapper")
public class BehaviorApplication {


    public static void main(String[] args) {
        SpringApplication.run(BehaviorApplication.class, args);
    }


    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
