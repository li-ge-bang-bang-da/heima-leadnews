package com.heima.behavior.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.behavior.service.ApArticleRelationService;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApLikesBehaviorService;
import com.heima.behavior.service.ApUnlikesBehaviorService;
import com.heima.model.behavior.dtos.ApArticleRelationDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ApArticleRelationServiceImpl implements ApArticleRelationService {

    @Autowired
    ApBehaviorEntryService apBehaviorEntryService;

    @Autowired
    ApLikesBehaviorService apLikesBehaviorService;

    @Autowired
    ApUnlikesBehaviorService apUnlikesBehaviorService;
    /**
     * 查询用户文章的点赞\不喜欢
     *
     * @param dto
     * @return
     */
    @Override
    public Map findApArticleRelation(ApArticleRelationDto dto) {
        //预封装结果
        Map<String,Object> map = new HashMap<>();
        map.put("islike",false);
        map.put("isunlike",false);

        //参数校验
        if (dto == null || dto.getArticleId() == null || dto.getEntryId() == null){
            return map;
        }

        //查询行为实体
        ApBehaviorEntry behaviorEntry = apBehaviorEntryService.findByUserIdOrEquipmentId(dto.getEntryId(),null);
        if (behaviorEntry == null){
            return map;
        }
        //查询文章点赞
        ApLikesBehavior likesBehavior = apLikesBehaviorService.getOne(Wrappers.<ApLikesBehavior>lambdaQuery()
                .eq(ApLikesBehavior::getArticleId, dto.getArticleId())
                .eq(ApLikesBehavior::getEntryId, behaviorEntry.getId())
                .eq(ApLikesBehavior::getType,ApLikesBehavior.Type.ARTICLE.getCode())
                .eq(ApLikesBehavior::getOperation,ApLikesBehavior.Operation.LIKE.getCode())
        );
        if (likesBehavior != null){
            map.put("islike",true);
        }

        //查询文章不喜欢
        ApUnlikesBehavior unlikesBehavior = apUnlikesBehaviorService.getOne(Wrappers.<ApUnlikesBehavior>lambdaQuery()
                .eq(ApUnlikesBehavior::getArticleId, dto.getArticleId())
                .eq(ApUnlikesBehavior::getEntryId, behaviorEntry.getId())
                .eq(ApUnlikesBehavior::getType,ApUnlikesBehavior.Type.UNLIKE.getCode())
        );
        if (unlikesBehavior != null){
            map.put("isunlike",true);
        }

        map.put("entryId",behaviorEntry.getId());

        return map;
    }
}
