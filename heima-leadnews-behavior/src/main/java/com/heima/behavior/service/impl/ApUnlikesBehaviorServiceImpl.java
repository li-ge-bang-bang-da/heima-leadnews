package com.heima.behavior.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApUnlikesBehaviorMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApUnlikesBehaviorService;
import com.heima.model.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Description:
 * @Version: V1.0
 */
@Service
public class ApUnlikesBehaviorServiceImpl extends ServiceImpl<ApUnlikesBehaviorMapper, ApUnlikesBehavior>
        implements ApUnlikesBehaviorService {

    @Autowired
    ApBehaviorEntryService apBehaviorEntryService;

    /**
     * 保存不喜欢行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult unLike(UnLikesBehaviorDto dto) {
        //校验参数
        if (dto == null || dto.getArticleId() == null
                || (dto.getType() < 0 || dto.getType() > 2)){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询行为实体
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        ApBehaviorEntry behaviorEntry = apBehaviorEntryService.findByUserIdOrEquipmentId(user.getId(), dto.getEquipmentId());
        if (behaviorEntry == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //查询不喜欢行为是否存在
        ApUnlikesBehavior unlikesBehavior = getOne(Wrappers.<ApUnlikesBehavior>lambdaQuery()
                .eq(ApUnlikesBehavior::getArticleId, dto.getArticleId())
                .eq(ApUnlikesBehavior::getEntryId, behaviorEntry.getEntryId())
        );

        //不存在则判断是否是不喜欢行为
//        if (unlikesBehavior == null && dto.getType().equals(ApUnlikesBehavior.Type.CANCEL.getCode())){
//            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"暂无不喜欢");
//        }
//        if (unlikesBehavior != null && dto.getType().equals(ApUnlikesBehavior.Type.UNLIKE.getCode())
//                && unlikesBehavior.getType().shortValue()== 0
//                && unlikesBehavior.getType().shortValue() == ApUnlikesBehavior.Type.UNLIKE.getCode()
//        ){
//            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"请勿取消不喜欢");
//        }

        //保存不喜欢行为
        if (unlikesBehavior == null){
            unlikesBehavior = new ApUnlikesBehavior();
            unlikesBehavior.setArticleId(dto.getArticleId());
            unlikesBehavior.setCreatedTime(new Date());
            unlikesBehavior.setEntryId(behaviorEntry.getId());
            unlikesBehavior.setType(dto.getType());
            save(unlikesBehavior);
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }else {     //取消不喜欢行为
            unlikesBehavior.setType(dto.getType());
            updateById(unlikesBehavior);
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
    }
}
