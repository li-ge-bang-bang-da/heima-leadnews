package com.heima.behavior.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApLikesBehaviorMapper;
import com.heima.behavior.mapper.ApReadBehaviorMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApLikesBehaviorService;
import com.heima.behavior.service.ApReadBehaviorService;
import com.heima.model.behavior.dtos.ReadBehaviorDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.behavior.pojos.ApReadBehavior;
import com.heima.model.common.constants.HotArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * @Description:
 * @Version: V1.0
 */
@Service
public class ApReadBehaviorServiceImpl extends ServiceImpl<ApReadBehaviorMapper, ApReadBehavior>
        implements ApReadBehaviorService {
    @Autowired
    ApBehaviorEntryService apBehaviorEntryService;

    @Autowired
    KafkaTemplate kafkaTemplate;

    /**
     * 保存或更新阅读行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult readBehavior(ReadBehaviorDto dto) {

        //参数校验
        if (dto == null || dto.getArticleId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询行为实体
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        ApBehaviorEntry behaviorEntry = apBehaviorEntryService.findByUserIdOrEquipmentId(user.getId(), dto.getEquipmentId());
        if (behaviorEntry == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        ApReadBehavior readBehavior = getOne(Wrappers.<ApReadBehavior>lambdaQuery()
                .eq(ApReadBehavior::getArticleId, dto.getArticleId())
                .eq(ApReadBehavior::getEntryId, behaviorEntry.getId())
        );

        //查询阅读行为是否存在
        //不存在新增
        if (readBehavior == null){
            readBehavior = new ApReadBehavior();
            readBehavior.setArticleId(dto.getArticleId());
            readBehavior.setCreatedTime(new Date());
            readBehavior.setEntryId(behaviorEntry.getId());
            readBehavior.setCount(dto.getCount());
            readBehavior.setLoadDuration(dto.getLoadDuration());
            readBehavior.setPercentage(dto.getPercentage());
            readBehavior.setReadDuration(dto.getReadDuration());
            save(readBehavior);
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

        }else {     //存在则修改阅读次数 +1

            readBehavior.setCount((short) (dto.getCount()+1));
            readBehavior.setUpdatedTime(new Date());
            readBehavior.setReadDuration(dto.getReadDuration());
            readBehavior.setPercentage(dto.getPercentage());
            readBehavior.setLoadDuration(dto.getLoadDuration());
            readBehavior.setEntryId(behaviorEntry.getId());
            updateById(readBehavior);

            //发消息给kafkaStream 实时计算
            //**** 文章阅读-发送消息
            UpdateArticleMess mess = new UpdateArticleMess();
            mess.setType(UpdateArticleMess.UpdateArticleType.VIEWS);
            mess.setArticleId(dto.getArticleId());
            kafkaTemplate.send(HotArticleConstants.HOTARTICLE_SCORE_INPUT_TOPIC,
                    UUID.randomUUID().toString(), JSON.toJSONString(mess));

            //返回结果
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

        }
    }
}
