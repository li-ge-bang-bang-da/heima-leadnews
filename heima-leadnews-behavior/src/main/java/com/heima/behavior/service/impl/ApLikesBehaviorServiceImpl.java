package com.heima.behavior.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApLikesBehaviorMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApLikesBehaviorService;
import com.heima.model.behavior.dtos.LikesBehaviorDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.common.constants.HotArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * @Description:
 * @Version: V1.0
 */
@Service
public class ApLikesBehaviorServiceImpl extends ServiceImpl<ApLikesBehaviorMapper, ApLikesBehavior>
        implements ApLikesBehaviorService {

    @Autowired
    ApBehaviorEntryService apBehaviorEntryService;

    @Autowired
    KafkaTemplate kafkaTemplate;

    /**
     * 保存点赞行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult like(LikesBehaviorDto dto) {
        //参数校验
        if (dto == null || dto.getArticleId() == null
                || (dto.getType() < 0 || dto.getType() > 2)
                || (dto.getOperation() < 0 || dto.getOperation() > 1)){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询行为实体  主键
        ApUser user = AppThreadLocalUtils.getUser();
        ApBehaviorEntry behaviorEntry = apBehaviorEntryService.findByUserIdOrEquipmentId(user.getId(), dto.getEquipmentId());
        if (behaviorEntry == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //查询点赞行为是否存在
        ApLikesBehavior likesBehavior = getOne(Wrappers.<ApLikesBehavior>lambdaQuery()
                .eq(ApLikesBehavior::getEntryId, behaviorEntry.getId())
                .eq(ApLikesBehavior::getArticleId, dto.getArticleId())
        );

        //不存在，判断是否是点赞行为
        if (likesBehavior == null && dto.getOperation().equals(ApLikesBehavior.Operation.CANCEL.getCode())){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"暂无点赞");
        }
        if (likesBehavior != null && dto.getOperation().equals(ApLikesBehavior.Operation.LIKE.getCode())
                && likesBehavior.getOperation().shortValue() == 0
                && likesBehavior.getOperation().shortValue() == ApLikesBehavior.Operation.LIKE.getCode()
        ){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"请勿重复点赞");
        }

        //保存点赞
        if (likesBehavior == null && ApLikesBehavior.Operation.LIKE.getCode() == dto.getOperation().shortValue()){
            likesBehavior = new ApLikesBehavior();
            likesBehavior.setArticleId(dto.getArticleId());
            likesBehavior.setCreatedTime(new Date());
            likesBehavior.setEntryId(behaviorEntry.getId());
            likesBehavior.setOperation(dto.getOperation());
            likesBehavior.setType(dto.getType());
            save(likesBehavior);

        }else {     //取消点赞
            likesBehavior.setOperation(dto.getOperation());
            updateById(likesBehavior);
        }
        //发消息给kafkaStream 实时计算
        if(dto.getOperation() ==0){
            UpdateArticleMess mess = new UpdateArticleMess();
            mess.setType(UpdateArticleMess.UpdateArticleType.LIKES);
            mess.setArticleId(dto.getArticleId());
            kafkaTemplate.send(HotArticleConstants.HOTARTICLE_SCORE_INPUT_TOPIC, UUID.randomUUID().toString(), JSON.toJSONString(mess));
        }

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

    }
}
