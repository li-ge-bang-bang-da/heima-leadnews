package com.heima.behavior.service;

import com.heima.model.behavior.dtos.ApArticleRelationDto;

import java.util.Map;

/**
 * @Description:
 * @Version: V1.0
 */
public interface ApArticleRelationService {
    /**
     * 查询用户文章的点赞\不喜欢
     *
     * @return
     */
    public Map findApArticleRelation(ApArticleRelationDto dto);
}