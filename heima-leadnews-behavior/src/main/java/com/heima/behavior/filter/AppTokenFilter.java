package com.heima.behavior.filter;

import com.heima.model.user.pojos.ApUser;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import com.heima.utils.threadlocal.WmThreadLocalUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
@WebFilter(urlPatterns = "/**" ,filterName = "AppTokenFilter")
public class AppTokenFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        String userId = ((HttpServletRequest) request).getHeader("userId");

        //如果userId为0，说明当前设备没有登录
        if (userId !=null && Integer.valueOf(userId).intValue() != 0){
            ApUser apUser = new ApUser();
            apUser.setId(Integer.valueOf(userId));

            // 保存到当前线程中
            AppThreadLocalUtils.setUser(apUser);
        }

        chain.doFilter(httpServletRequest,httpServletResponse);

    }
}
