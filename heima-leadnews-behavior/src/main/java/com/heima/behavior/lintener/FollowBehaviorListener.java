package com.heima.behavior.lintener;


import com.alibaba.fastjson.JSON;
import com.heima.behavior.service.ApFollowBehaviorService;
import com.heima.model.behavior.dtos.FollowBehaviorDto;
import com.heima.model.common.constants.FollowBehaviorConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FollowBehaviorListener {

    @Autowired
    ApFollowBehaviorService apFollowBehaviorService;

    @KafkaListener(topics = FollowBehaviorConstants.FOLLOW_BEHAVIOR_TOPIC)
    public void recevieMessage(String message){
        log.info("FollowBehaviorListener recevieMessage :{}",message);
        if (StringUtils.isNotBlank(message)){
            apFollowBehaviorService.saveFollowBehavior(JSON.parseObject(message, FollowBehaviorDto.class));

            log.info("FollowBehaviorListener success");
        }
    }
}
