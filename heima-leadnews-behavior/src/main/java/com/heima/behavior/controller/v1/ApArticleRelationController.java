package com.heima.behavior.controller.v1;

import com.heima.apis.behavior.ApArticleRelationControllerApi;
import com.heima.behavior.service.ApArticleRelationService;
import com.heima.model.behavior.dtos.ApArticleRelationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("/api/v1/article_relation")
public class ApArticleRelationController implements ApArticleRelationControllerApi {

    @Autowired
    ApArticleRelationService apArticleRelationService;
    /**
     * 查询用户文章的点赞\不喜欢
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/one")
    public Map findApArticleRelation(@RequestBody ApArticleRelationDto dto) {
        return apArticleRelationService.findApArticleRelation(dto);
    }
}
