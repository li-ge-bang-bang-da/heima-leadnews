package com.heima.apis.article;

import com.heima.model.article.dtos.CollectionBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface CollectionControllerApi {

    /**
     * 保存收藏行为
     * @param dto
     * @return
     */
    ResponseResult collection(CollectionBehaviorDto dto);
}
