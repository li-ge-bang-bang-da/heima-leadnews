package com.heima.apis.article;

import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PathVariable;

public interface AuthorControllerApi {

    /**
     * 根据用户id查询
     * @param UserId
     * @return
     */
    public ApAuthor findByUserId(Integer UserId);

    /**
     * 新增作者
     * @param apAuthor
     * @return
     */
    public ResponseResult save(ApAuthor apAuthor);


    /**
     * 根据Id查询作者
     * @param id
     * @return
     */
    public ApAuthor findById(@PathVariable("id") Integer id);
}
