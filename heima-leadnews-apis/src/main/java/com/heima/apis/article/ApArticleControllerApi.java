package com.heima.apis.article;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApArticleControllerApi {

    /**
     * 保存app文章三张表
     * @param articleDto
     * @return  返回文章的ID
     */
    ResponseResult saveArticle(ArticleDto articleDto);
}
