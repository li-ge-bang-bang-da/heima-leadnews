package com.heima.apis.behavior;

import com.heima.model.behavior.dtos.ApArticleRelationDto;

import java.util.Map;

public interface ApArticleRelationControllerApi {

    /**
     * 查询用户文章的点赞\不喜欢
     * @return
     */
    public Map findApArticleRelation(ApArticleRelationDto dto);
}