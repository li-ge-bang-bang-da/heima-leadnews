package com.heima.apis.behavior;

import com.heima.model.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApUnlikesBehaviorControllerApi {

    /**
     *  保存不喜欢行为
     * @param dto
     * @return
     */
    ResponseResult unLike(UnLikesBehaviorDto dto);
}
