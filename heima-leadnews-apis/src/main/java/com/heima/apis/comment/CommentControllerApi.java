package com.heima.apis.comment;

import com.heima.model.comment.CommentDto;
import com.heima.model.comment.CommentLikeDto;
import com.heima.model.comment.CommentSaveDto;
import com.heima.model.common.dtos.ResponseResult;

public interface CommentControllerApi {

    /**
     * 保存评论
     * @param dto
     * @return
     */
    public ResponseResult saveComment(CommentSaveDto dto);

    /**
     * 点赞某一条评论
     * @param dto
     * @return
     */
    public ResponseResult like(CommentLikeDto dto);

    /**
     * 查询评论
     * @param dto
     * @return
     */
    public ResponseResult findByArticleId(CommentDto dto);
}
