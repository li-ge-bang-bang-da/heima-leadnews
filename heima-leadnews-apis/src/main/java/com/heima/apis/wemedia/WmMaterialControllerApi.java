package com.heima.apis.wemedia;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartFile;

public interface WmMaterialControllerApi {

    /**
     * 图片上传
     * @param multipartFile
     * @return
     */
    public ResponseResult uploadPicture(MultipartFile multipartFile);

    /**
     * 查询图片列表
     * @param dto
     * @return
     */
    public ResponseResult findAll(WmMaterialDto dto);

    /**
     * 删除图片
     * @param id
     * @return
     */
    public ResponseResult delete(Integer id);

    /**
     * 取消收藏
     * @param id
     * @return
     */
    @ApiOperation("取消收藏")
    ResponseResult cancelCollectionMaterial(Integer id);

    /**
     * 收藏图片
     * @param id
     * @return
     */
    @ApiOperation("收藏图片")
    ResponseResult collectionMaterial(Integer id);

}
