package com.heima.apis.wemedia;

import com.heima.model.wemedia.pojos.WmUser;

public interface WmUserControllerApi {

    /**
     * 根据登录名查询当前用户
     * @return
     */
    public WmUser findByName(String name);

    /**
     * 自媒体用户新增
     * @param wmUser
     * @return
     */
    public WmUser save(WmUser wmUser);

    /**
     * 根据id查询自媒体用户
     * @param id 自媒体用户ID
     * @return
     */
    WmUser findWmUserById(Integer id);
}
