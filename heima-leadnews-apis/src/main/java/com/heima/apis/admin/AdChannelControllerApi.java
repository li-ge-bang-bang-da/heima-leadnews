package com.heima.apis.admin;

import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;

public interface AdChannelControllerApi {

    /**
     * 根据名称分页查询频道列表
     * @param dto
     * @return
     */

    public ResponseResult findByNameAndPage(ChannelDto dto);

    /**
     * 新增
     * @param adchannel
     * @return
     */
    public ResponseResult insert(AdChannel adchannel);

    /**
     * 修改
     * @param adChannel
     * @return
     */
    public ResponseResult update(AdChannel adChannel);


    /**
     * 删除
     * @param id
     * @return
     */
    public ResponseResult deleteById(Integer id);


    /**
     * 查询所有频道
     * @return
     */
    public ResponseResult findAll();
}