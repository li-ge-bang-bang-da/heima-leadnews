package com.heima.apis.admin;

import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;

public interface LoginControllerApi {

    /**
     * 管理员用户登录
     * @param dto
     * @return
     */
    public ResponseResult login(AdUserDto dto);

    /**
     * 管理员用户注册
     * @param adUser
     * @return
     */

    public ResponseResult register(AdUser adUser);
}
