package com.heima.apis.user;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.UserRelationDto;

public interface UserRelationControllerApi {

    /**
     * 关注或取消关注
     * @param dto  关注 0  取关 1
     * @return
     */
    ResponseResult follow(UserRelationDto dto);
}
