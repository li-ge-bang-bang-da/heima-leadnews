package com.heima.es.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticle;

public interface ApArticleMapper extends BaseMapper<ApArticle> {

}
