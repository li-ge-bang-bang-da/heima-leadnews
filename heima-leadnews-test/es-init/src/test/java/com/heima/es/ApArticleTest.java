package com.heima.es;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.heima.es.mapper.ApArticleMapper;
import com.heima.es.vos.SearchArticleVo;
import com.heima.model.article.pojos.ApArticle;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

@SpringBootTest(classes = EsInitApplication.class)
@RunWith(SpringRunner.class)
public class ApArticleTest {
    /**
     * 全量导入索引库
     */

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Autowired
    ApArticleMapper apArticleMapper;

    @Test
    public void initTest() throws IOException {
        //分页查询第一页数据
//        System.out.println("开始导入......");
//        IPage<ApArticle> page = new Page<>(1, 500);
//        IPage<ApArticle> pageResult = apArticleMapper.selectPage(page, null);
//        long pages = pageResult.getPages();
//
//        //批量导入对象
//        BulkRequest bulkRequest = new BulkRequest("app_info_article");
//        //遍历页码批量插入数据库
//        for (long i = 1; i <= pages; i++) {
//            //如果数据足够多，则从第二页插入
//            if (i > 1) {
//                page = new Page<>(i, 500);
//                pageResult = apArticleMapper.selectPage(page, null);
//            }
//            //每一页的数据
//            List<ApArticle> articleList = pageResult.getRecords();
//
//            for (ApArticle article : articleList) {
//                //封装数据到vo中
//                SearchArticleVo searchArticleVo = new SearchArticleVo();
//                BeanUtils.copyProperties(article, searchArticleVo);
//
//                //设置文档ID
//                IndexRequest indexRequest = new IndexRequest();
//                indexRequest.id(article.getId().toString());
//                indexRequest.source(JSON.toJSONString(searchArticleVo), XContentType.JSON);
//
//                bulkRequest.add(indexRequest);
//            }
//
//            //批量导入
//            BulkResponse responses = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
//            System.out.println(responses.status());
//            System.out.println("插入第 " + i + " 页成功");
//        }
    }
}
