package com.itheima.kafka.listener;

import com.alibaba.fastjson.JSON;
import com.itheima.kafka.controller.HelloController;
import com.itheima.kafka.pojo.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class HelloListener {

    @KafkaListener(topics = HelloController.TOPIC)
    public void receiverMessage(String message) {
        if (!StringUtils.isEmpty(message)) {

            User user = JSON.parseObject(message, User.class);
            System.out.println(user.getUsername());
            System.out.println(user.getAge());
//            System.out.println(message);
        }
    }
}
