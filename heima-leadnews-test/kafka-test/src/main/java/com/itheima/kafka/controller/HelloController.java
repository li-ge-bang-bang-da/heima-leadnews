package com.itheima.kafka.controller;

import com.alibaba.fastjson.JSON;
import com.itheima.kafka.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    public static final String TOPIC ="itcast-heima-boot";

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @GetMapping("/hello")
    public String Hello(){

        User user = new User();
        user.setUsername("李四");
        user.setAge(18);
        //第一个参数：topics
        //第二个参数：消息内容
        kafkaTemplate.send(TOPIC, JSON.toJSONString(user));
        return "ok";
    }

}
