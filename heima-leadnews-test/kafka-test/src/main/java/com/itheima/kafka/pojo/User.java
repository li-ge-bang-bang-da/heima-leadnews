package com.itheima.kafka.pojo;

import lombok.Data;

@Data
public class User {
    private String username;
    private Integer age;
}
