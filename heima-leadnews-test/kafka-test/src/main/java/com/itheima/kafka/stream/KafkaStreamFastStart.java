package com.itheima.kafka.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.ValueMapper;

import java.util.Arrays;
import java.util.Properties;

public class KafkaStreamFastStart {

//    public static void main(String[] args) {
//
//        //1 kafka配置信息
//        Properties prop = new Properties();
//        prop.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.200.129:9092");
//        prop.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
//        prop.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
//        prop.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-sample");
//
//        //构建Streambuilder
//        StreamsBuilder builder = new StreamsBuilder();
//
//        //流式计算
//        streamProcessor(builder);
//
//        //创建kafkaStream
//        KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), prop);
//
//        //开启流式计算
//        kafkaStreams.start();
//    }
//
//    private static void streamProcessor(StreamsBuilder builder) {
//        KStream<String, String> stream = builder.stream("itcast-topic-input");
//        stream.flatMapValues(new ValueMapper<String, Iterable<String>>() {
//            @Override
//            public Iterable<String> apply(String value) {
//                //接收消息的具体内容
//                System.out.println("消息内容：" + value);
//                return  Arrays.asList(value.split(" "));
//            }
//        })
//                //根据value分组
//                .groupBy((key,value)->value)
//                //时间间隔窗口
//                .windowedBy(TimeWindows.of(5000))
//                //聚合查询单词个数
//                .count()
//                //转成kStream
//                .toStream()
//                //处理后结果key和value转成string
//                .map((key,value)->{
//                    KeyValue<String, String> keyValue = new KeyValue<>(key.key().toString(), value.toString());
//                    return keyValue;
//                })
//                //处理后发送给消费方
//                .to("itcast-topic-output");
//    }
}
