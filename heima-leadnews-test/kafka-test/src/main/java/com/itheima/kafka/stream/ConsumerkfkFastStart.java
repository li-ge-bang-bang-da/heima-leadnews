package com.itheima.kafka.stream;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

/**
 * 消息消费者
 */
public class ConsumerkfkFastStart {

    private static final String TOPIC = "itcast-topic-output";

//    public static void main(String[] args) {
//
//        //添加配置信息
//        Properties properties = new Properties();
//        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.200.129:9092");
//        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
//        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
//        //设置分组
//        properties.put(ConsumerConfig.GROUP_ID_CONFIG,"group1");
//
//        //创建消费者
//        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
//        //订阅主题
//        consumer.subscribe(Collections.singletonList(TOPIC));
//
//        System.out.println("消费方获取处理后结果：");
//        while (true){
//            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
//            for (ConsumerRecord record : records) {
//                System.out.println(record.key()+": "+record.value());
//            }
//        }
//
//    }
}