package com.heima.article.service;

import com.heima.model.article.dtos.CollectionBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface CollectionService {

    /**
     * 保存收藏行为
     * @param dto
     * @return
     */
    ResponseResult collection(CollectionBehaviorDto dto);}
