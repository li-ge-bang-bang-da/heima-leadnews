package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.mess.ArticleVisitStreamMess;

public interface ApArticleService extends IService<ApArticle> {

    /**
     * 保存app文章三张表
     * @param articleDto
     * @return  返回文章的ID
     */
    ResponseResult saveArticle(ArticleDto articleDto);

    /**
     * 根据参数加载文章列表
     * @param loadtype 1为加载更多  2为加载最新
     * @param dto
     * @return
     */
    ResponseResult load(Short loadtype, ArticleHomeDto dto);

    /**
     * 根据参数加载文章列表  v2
     * @param loadtype
     * @param dto
     * @param firstPage
     * @return
     */
    public ResponseResult load2(Short loadtype, ArticleHomeDto dto,boolean firstPage);

    /**
     * 加载文章详情
     * @param dto
     * @return
     */
    public ResponseResult loadArticleInfo(ArticleInfoDto dto);

    /**
     * 加载文章详情的初始化配置信息，比如关注、喜欢、不喜欢等
     * @param dto
     * @return
     */
    ResponseResult loadArticleBehavior(ArticleInfoDto dto);

    /**
     * 更新文章数据库行为数据，更新redis
     * @param articleVisitStreamMess
     */
    void updateApArticle(ArticleVisitStreamMess articleVisitStreamMess);
}