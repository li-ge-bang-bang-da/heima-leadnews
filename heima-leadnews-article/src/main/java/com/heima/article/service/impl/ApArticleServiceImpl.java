package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.*;
import com.heima.article.service.ApArticleService;
import com.heima.common.exception.CustException;
import com.heima.feigns.behavior.BehaviorFeign;
import com.heima.feigns.user.UserFeign;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.*;
import com.heima.model.article.vos.HotArticleVo;
import com.heima.model.behavior.dtos.ApArticleRelationDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.common.constants.ArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserFollow;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {

    @Autowired
    AuthorMapper authorMapper;

    @Autowired
    ApArticleConfigMapper apArticleConfigMapper;

    @Autowired
    ApArticleContentMapper apArticleContentMapper;

    @Autowired
    ApArticleMapper apArticleMapper;

    @Value("${file.oss.web-site}")
    private String webSite;


    @Autowired
    UserFeign userFeign;

    @Autowired
    BehaviorFeign behaviorFeign;

    @Autowired
    CollectionMapper collectionMapper;

    @Autowired
    KafkaTemplate kafkaTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 根据参数加载文章列表  v2
     *
     * @param loadtype
     * @param dto
     * @param firstPage
     * @return
     */
    @Override
    public ResponseResult load2(Short loadtype, ArticleHomeDto dto, boolean firstPage) {
        if (firstPage){
            String hotArticleList = (String) redisTemplate.opsForValue().get(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + dto.getTag());
            List<HotArticleVo> hotArticleVos = JSON.parseArray(hotArticleList, HotArticleVo.class);
            ResponseResult result = ResponseResult.okResult(hotArticleVos);
            result.setHost(webSite);
            return result;
        }

        return load(loadtype,dto);
    }

    /**
     * 更新文章数据库行为数据，更新redis
     *
     * @param articleVisitStreamMess
     */
    @Override
    public void updateApArticle(ArticleVisitStreamMess articleVisitStreamMess) {
        Long articleId = articleVisitStreamMess.getArticleId();
        ApArticle apArticle = getById(articleId);
        if (apArticle == null) {
            log.info("updateApArticle gteById ApArticle is null, id:{}", articleId);
            return;
        }
        //更新数据库
        if (articleVisitStreamMess.getView() != 0) {
            int view = (int) (apArticle.getViews() == null ? articleVisitStreamMess.getView() : articleVisitStreamMess.getView() + apArticle.getViews());
            apArticle.setViews(view);
        }
        if (articleVisitStreamMess.getLike() != 0) {
            int like = (int) (apArticle.getLikes() == null ? articleVisitStreamMess.getLike() : articleVisitStreamMess.getLike() + apArticle.getLikes());
            apArticle.setLikes(like);
        }
        if (articleVisitStreamMess.getComment() != 0) {
            int comment = (int) (apArticle.getComment() == null ? articleVisitStreamMess.getComment() : articleVisitStreamMess.getComment() + apArticle.getComment());
            apArticle.setComment(comment);
        }
        if (articleVisitStreamMess.getCollect() != 0) {
            int collection = (int) (apArticle.getCollection() == null ? articleVisitStreamMess.getCollect() : articleVisitStreamMess.getCollect() + apArticle.getCollection());
            apArticle.setCollection(collection);
        }
        updateById(apArticle);

        //计算文章分值
        int score = computeScore(apArticle);
        score = score * ArticleConstants.HOT_ARTICLE_SCORE;

        //更新频道的redis数据库
        String articleListStr = (String) redisTemplate.opsForValue().get(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + apArticle.getChannelId());
        updateArticleCache(apArticle, score, articleListStr, ArticleConstants.HOT_ARTICLE_FIRST_PAGE + apArticle.getChannelId());

        //更新推荐的redis数据库
        String articleDefaultStr = (String) redisTemplate.opsForValue().get(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + ArticleConstants.DEFAULT_TAG);
        updateArticleCache(apArticle, score, articleDefaultStr, ArticleConstants.HOT_ARTICLE_FIRST_PAGE + ArticleConstants.DEFAULT_TAG);
    }

    /**
     * 更新redis热点文章缓存数据
     *
     * @param apArticle   当前文章
     * @param score       分值
     * @param hotArticles redis中缓存热点文章
     * @param tag         key
     */
    private void updateArticleCache(ApArticle apArticle, int score, String hotArticles, String tag) {
        if (StringUtils.isNotBlank(hotArticles)) {
            //获取热点列表
            List<HotArticleVo> hotArticleVos = JSON.parseArray(hotArticles, HotArticleVo.class);
            boolean flag = false;
            //如果列表中有当前文章，则更新分值
            for (HotArticleVo hotArticleVo : hotArticleVos) {
                if (apArticle.getId().equals(hotArticleVo.getId())) {
                    flag = true;
                    hotArticleVo.setScore(hotArticleVo.getScore() + score);
                }
            }
            //如果没有当前文章
            if (!flag) {
                HotArticleVo articleVo = new HotArticleVo();
                BeanUtils.copyProperties(apArticle, articleVo);
                articleVo.setScore(score);

                if (hotArticleVos.size() > 30) {
                    //缓存频道下的最后一条文章的分值小于当前文章分值 则替换
                    HotArticleVo lastHotArticleVo = hotArticleVos.get(hotArticleVos.size() - 1);
                    if (score > lastHotArticleVo.getScore()) {
                        hotArticleVos.remove(lastHotArticleVo);     //移除最后一条
                        hotArticleVos.add(articleVo);       //新增
                    }
                } else {     //直接新增
                    hotArticleVos.add(articleVo);
                }
            }
            //分值倒序排序
            hotArticleVos = hotArticleVos.stream().sorted(Comparator.comparing(HotArticleVo::getScore).reversed())
                    .collect(Collectors.toList());

            //保存到redis
            redisTemplate.opsForValue().set(tag, JSON.toJSONString(hotArticleVos));
            log.info("updateApArticle updateArticleCache success");
        }
    }

    /**
     * 计算分值
     *
     * @param apArticle
     * @return
     */
    private int computeScore(ApArticle apArticle) {
        int score = 0;
        // 阅读 1
        if (apArticle.getViews() != null) {
            score += apArticle.getViews() * ArticleConstants.HOT_ARTICLE_VIEW_WEIGHT;
        }
        // 点赞 3
        if (apArticle.getLikes() != null) {
            score += apArticle.getLikes() * ArticleConstants.HOT_ARTICLE_LIKE_WEIGHT;
        }
        // 评论 5
        if (apArticle.getComment() != null) {
            score += apArticle.getComment() * ArticleConstants.HOT_ARTICLE_COMMENT_WEIGHT;
        }
        // 收藏 8
        if (apArticle.getCollection() != null) {
            score += apArticle.getCollection() * ArticleConstants.HOT_ARTICLE_COLLECTION_WEIGHT;
        }

        return score;
    }

    /**
     * 加载文章详情的初始化配置信息，比如关注、喜欢、不喜欢等
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult loadArticleBehavior(ArticleInfoDto dto) {
        //参数校验
        if (dto == null || dto.getArticleId() == null || dto.getAuthorId() == null) {
            return ResponseResult.okResult(new HashMap<>());
        }
        boolean iscollection = false;
        boolean isfollow = false;

        //获取当前登录用户
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.okResult(new HashMap<>());
        }
        //查询作者 获取ID
        ApAuthor apAuthor = authorMapper.selectById(dto.getAuthorId());
        if (apAuthor == null) {
            return ResponseResult.okResult(new HashMap<>());
        }
        //是否关注
        ApUserFollow userFollow = userFeign.findByUserIdAndFollowId(user.getId(), apAuthor.getUserId());
        if (userFollow != null) {
            isfollow = true;
        }
        //是否点赞/不喜欢
        ApArticleRelationDto relationDto = new ApArticleRelationDto();
        relationDto.setArticleId(dto.getArticleId());
        relationDto.setEntryId(user.getId());
        relationDto.setType((short) 1);
        Map behaviorMap = behaviorFeign.findApArticleRelation(relationDto);


        //是否收藏
        if (behaviorMap.get("entryId") != null) {
            ApCollection apCollection = collectionMapper.selectOne(Wrappers.<ApCollection>lambdaQuery()
                    .eq(ApCollection::getArticleId, dto.getArticleId())
                    .eq(ApCollection::getEntryId, behaviorMap.get("entryId"))
                    .eq(ApCollection::getType, ApCollection.Type.ARTICLE.getCode())
            );
            if (apCollection != null) {
                iscollection = true;
            }
        }

        //返回结果
        Map<String, Object> map = new HashMap<>();
        map.put("isfollow", isfollow);
        map.put("iscollection", iscollection);
        map.putAll(behaviorMap);

        return ResponseResult.okResult(map);
    }

    /**
     * 加载文章详情
     *
     * @param dto 文章ID
     * @return
     */
    @Override
    public ResponseResult loadArticleInfo(ArticleInfoDto dto) {
        //参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //查询文章配置信息
        Long articleId = dto.getArticleId();
        ApArticleConfig apArticleConfig = apArticleConfigMapper.selectOne(Wrappers.<ApArticleConfig>lambdaQuery()
                .eq(ApArticleConfig::getArticleId, articleId));
        if (apArticleConfig == null || apArticleConfig.getIsDelete() || apArticleConfig.getIsDown()) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章已下架");
        }

        //查询文章内容
        ApArticleContent apArticleContent = apArticleContentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery()
                .eq(ApArticleContent::getArticleId, articleId));
        if (apArticleContent == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章不存在");
        }

        //封装返回结果
        Map<String, Object> map = new HashMap<>();
        map.put("config", apArticleConfig);
        map.put("content", apArticleContent);

        return ResponseResult.okResult(map);
    }

    /**
     * 根据参数加载文章列表
     *
     * @param loadtype 1为加载更多  2为加载最新
     * @param dto
     * @return
     */
    @Override
    public ResponseResult load(Short loadtype, ArticleHomeDto dto) {
        //校验参数
        if (dto == null) {
            dto = new ArticleHomeDto();
        }

        //如果是设备登录，设置默认数据
        //一页条数
        Integer size = dto.getSize();
        if (size == null || size > 50) {
            size = Math.min(size, 50);
        }
        if (size <= 0) {
            size = 10;
        }
        //频道
        if (StringUtils.isBlank(dto.getTag())) {
            dto.setTag(ArticleConstants.DEFAULT_TAG);
        }
        //上拉
        if (dto.getMinBehotTime() == null) {
            dto.setMinBehotTime(new Date());
        }
        //下拉
        if (dto.getMaxBehotTime() == null) {
            dto.setMaxBehotTime(new Date());
        }
        //类型判断，如果是设备登录，默认设置查询更多
        if (!loadtype.equals(ArticleConstants.LOADTYPE_LOAD_MORE)
                && !loadtype.equals(ArticleConstants.LOADTYPE_LOAD_NEW)) {
            loadtype = ArticleConstants.LOADTYPE_LOAD_MORE;
        }
        //执行查询
        List<ApArticle> apArticles = apArticleMapper.loadArticleList(dto, loadtype);

        //返回结果
        ResponseResult result = ResponseResult.okResult(apArticles);
        result.setHost(webSite);
        return result;
    }

    /**
     * 保存app文章三张表
     *
     * @param articleDto
     * @return 返回文章的ID
     */
    @Override
    public ResponseResult saveArticle(ArticleDto articleDto) {

        //设置作者id
        ApArticle apArticle = new ApArticle();
        BeanUtils.copyProperties(articleDto, apArticle);
        String authorName = articleDto.getAuthorName();
        ApAuthor apAuthor = authorMapper.selectOne(Wrappers.<ApAuthor>lambdaQuery().eq(ApAuthor::getName, authorName));
        if (apAuthor == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "作者不存在");
        }
        apArticle.setAuthorId(apAuthor.getId().longValue());
        //补充其他属性Article
        if (articleDto.getId() == null) {  //新增
            apArticle.setComment(0);
            apArticle.setViews(0);
            apArticle.setCollection(0);
            apArticle.setLikes(0);

            //保存Article数据到数据库
            save(apArticle);

            //保存ArticleConfig数据到数据库
            ApArticleConfig apArticleConfig = new ApArticleConfig();
            apArticleConfig.setArticleId(apArticle.getId());
            apArticleConfig.setIsForward(true);
            apArticleConfig.setIsComment(true);
            apArticleConfig.setIsDown(false);
            apArticleConfig.setIsDelete(false);
            apArticleConfigMapper.insert(apArticleConfig);

            //保存ArticleContent数据到数据库
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setArticleId(apArticle.getId());
            apArticleContent.setContent(articleDto.getContent());
            apArticleContentMapper.insert(apArticleContent);

        } else {     //修改
            //先查后改
            ApArticle article = getById(articleDto.getId());
            if (article == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章不存在");
            }
            updateById(apArticle);

            ApArticleContent apArticleContent = apArticleContentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery()
                    .eq(ApArticleContent::getArticleId, articleDto.getId()));

            if (apArticleContent == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
            }
            apArticleContent.setContent(articleDto.getContent());
            apArticleContentMapper.updateById(apArticleContent);
        }

        //导入数据到es索引库  发送消息
        Map<String, Object> map = new HashMap<>();
        map.put("id", apArticle.getId().toString());
        map.put("publishTime", apArticle.getPublishTime());
        map.put("layout", apArticle.getLayout());
        map.put("images", apArticle.getImages());
        map.put("authorId", apArticle.getAuthorId());
        map.put("title", apArticle.getTitle());
        kafkaTemplate.send(ArticleConstants.ARTICLE_ES_SYNC_TOPIC, JSON.toJSONString(map));
        //返回封装结果 id
        return ResponseResult.okResult(apArticle.getId());
    }
}