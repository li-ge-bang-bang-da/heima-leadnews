package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.HotArticleService;
import com.heima.feigns.admin.AdminFeign;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.vos.HotArticleVo;
import com.heima.model.common.constants.ArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class HotArticleServiceImpl implements HotArticleService {

    @Autowired
    ApArticleMapper apArticleMapper;

    @Autowired
    AdminFeign adminFeign;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 计算热文章
     */
    @Override
    public void computeHotArticle() {
        //筛选前5天的文章
        String date = DateTime.now().minusDays(5).toString("yyyy-MM-dd 00:00:00");
        List<ApArticle> apArticleList = apArticleMapper.selectList(Wrappers.<ApArticle>lambdaQuery()
                .gt(ApArticle::getPublishTime, date));

        //计算热点文章分值
        List<HotArticleVo> hotArticleVoList = computeArticleScore(apArticleList);

        //每个频道缓存30条热点文章到redis
        cacheTagToRedis(hotArticleVoList);
    }

    /**
     * 添加热点文章到redis
     *
     * @param hotArticleVoList
     */
    private void cacheTagToRedis(List<HotArticleVo> hotArticleVoList) {
        //查询所有频道
        ResponseResult responseResult = adminFeign.findAll();
        if (responseResult.getCode() == 0) {
            //遍历频道列表
            List<AdChannel> channels = JSON.parseArray(JSON.toJSONString(responseResult.getData()), AdChannel.class);
            for (AdChannel channel : channels) {
                //缓存每个频道下的文章 --->30条
                List<HotArticleVo> hotArticleVos = hotArticleVoList.stream()
                        .filter(hotArticleVo -> hotArticleVo.getChannelId().equals(channel.getId()))
                        .collect(Collectors.toList());
                sortAndCache(hotArticleVos, ArticleConstants.HOT_ARTICLE_FIRST_PAGE + channel.getId());
            }
        }
        //给推荐频道缓存30条数据  所有文章排序之后的前30条
        sortAndCache(hotArticleVoList, ArticleConstants.HOT_ARTICLE_FIRST_PAGE + ArticleConstants.DEFAULT_TAG);
    }

    /**
     * 缓存热点文章到redis
     * @param hotArticleVos
     * @param key
     */
    private void sortAndCache(List<HotArticleVo> hotArticleVos, String key) {
        //对文章进行排序
        hotArticleVos = hotArticleVos.stream().sorted(Comparator.comparing(HotArticleVo::getScore)
                .reversed()).collect(Collectors.toList());
        if (hotArticleVos.size() > 30) {
            hotArticleVos = hotArticleVos.subList(0,30);
        }

        redisTemplate.opsForValue().set(key,JSON.toJSONString(hotArticleVos));
    }

    /**
     * 计算热点文章的分值
     *
     * @param apArticleList
     * @return
     */
    private List<HotArticleVo> computeArticleScore(List<ApArticle> apArticleList) {
        //定义返回的集合
        List<HotArticleVo> hotArticleVoList = new ArrayList<>();
        //遍历文章列表对文章计算分值

        for (ApArticle apArticle : apArticleList) {
            HotArticleVo hotArticleVo = new HotArticleVo();
            BeanUtils.copyProperties(apArticle, hotArticleVo);

            //计算分值
            Integer score = computeScore(apArticle);
            hotArticleVo.setScore(score);
            //添加到集合
            hotArticleVoList.add(hotArticleVo);
        }

        return hotArticleVoList;
    }

    /**
     * 计算文章分值算法
     *
     * @param apArticle
     * @return
     */
    private Integer computeScore(ApArticle apArticle) {

        int score = 0;
        //阅读
        if (apArticle.getViews() != null) {
            score += apArticle.getViews() * ArticleConstants.HOT_ARTICLE_VIEW_WEIGHT;
        }
        //点赞
        if (apArticle.getLikes() != null) {
            score += apArticle.getLikes() * ArticleConstants.HOT_ARTICLE_LIKE_WEIGHT;
        }
        //评论
        if (apArticle.getComment() != null) {
            score += apArticle.getComment() * ArticleConstants.HOT_ARTICLE_COMMENT_WEIGHT;
        }
        //收藏
        if (apArticle.getCollection() != null) {
            score += apArticle.getCollection() * ArticleConstants.HOT_ARTICLE_COLLECTION_WEIGHT;
        }

        return score;
    }
}
