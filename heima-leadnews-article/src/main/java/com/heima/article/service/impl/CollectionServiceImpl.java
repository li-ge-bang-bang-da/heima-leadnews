package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.CollectionMapper;
import com.heima.article.service.CollectionService;
import com.heima.feigns.behavior.BehaviorFeign;
import com.heima.model.article.dtos.CollectionBehaviorDto;
import com.heima.model.article.pojos.ApCollection;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.common.constants.HotArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class CollectionServiceImpl extends ServiceImpl<CollectionMapper, ApCollection>
        implements CollectionService {
    @Autowired
    BehaviorFeign behaviorFeign;

    @Autowired
    KafkaTemplate kafkaTemplate;

    /**
     * 保存收藏行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult collection(CollectionBehaviorDto dto) {
        //参数校验
        if (dto == null || (dto.getType() < 0 || dto.getType() > 2)
                || (dto.getOperation() < 0 || dto.getOperation() > 1)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询行为实体
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        ApBehaviorEntry behaviorEntry = behaviorFeign.findByUserIdOrEquipmentId(user.getId(), dto.getEquipmentId());
        if (behaviorEntry == null) {
            ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询收藏行为是否存在
        ApCollection collection = getOne(Wrappers.<ApCollection>lambdaQuery()
                .eq(ApCollection::getArticleId, dto.getEntryId())
                .eq(ApCollection::getEntryId, behaviorEntry.getId())
        );

        //不存在，判断是否是收藏行为
//        if (collection == null){
//            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"暂无收藏");
//        }
//        if (collection != null && dto.getOperation().equals(ApLikesBehavior.Operation.LIKE.getCode())
//                && likesBehavior.getOperation().shortValue() == 0
//                && likesBehavior.getOperation().shortValue() == ApLikesBehavior.Operation.LIKE.getCode()
//        ){
//            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"请勿重复点赞");
//        }


        //保存收藏
        if (collection == null && dto.getOperation() == 0) {
            collection = new ApCollection();
            collection.setArticleId(dto.getEntryId());
            collection.setCollectionTime(new Date());
            collection.setEntryId(behaviorEntry.getId());
            collection.setPublishedTime(new Date());
            collection.setType(dto.getType());
            save(collection);
        }
        //取消收藏
        if (collection != null && dto.getOperation() == 1) {
            removeById(collection.getId());
        }

        //发消息给kafkaStream 实时计算
        if(dto.getOperation() ==0){
            UpdateArticleMess mess = new UpdateArticleMess();
            mess.setType(UpdateArticleMess.UpdateArticleType.COLLECTION);
            mess.setArticleId(dto.getEntryId());
            kafkaTemplate.send(HotArticleConstants.HOTARTICLE_SCORE_INPUT_TOPIC, UUID.randomUUID().toString(), JSON.toJSONString(mess));
        }

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

}
