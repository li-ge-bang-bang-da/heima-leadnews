package com.heima.article.jobs;

import com.heima.article.service.HotArticleService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ComputeHotArticleJob {

    @Autowired
    HotArticleService hotArticleService;

    @XxlJob("computeHotArticleJob")
    public ReturnT<String> handle(String param){
        log.info("ComputeHotArticleJob handle is start............");

        hotArticleService.computeHotArticle();

        log.info("****************ComputeHotArticleJob handle is sucess****************");
        return ReturnT.SUCCESS;
    }
}
