package com.heima.article.controller.v1;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.apis.article.AuthorControllerApi;
import com.heima.article.service.AuthorService;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/author")
public class AuthorController implements AuthorControllerApi {

    @Autowired
    AuthorService authorService;
    /**
     * 根据用户id查询
     *
     * @param UserId
     * @return
     */
    @Override
    @GetMapping("/findByUserId/{userId}")
    public ApAuthor findByUserId(@PathVariable("userId") Integer UserId) {
        return authorService.getOne(Wrappers.<ApAuthor>lambdaQuery().eq(ApAuthor::getUserId,UserId));
    }

    /**
     * 新增作者
     *
     * @param apAuthor
     * @return
     */
    @Override
    @PostMapping("/save")
    public ResponseResult save(@RequestBody ApAuthor apAuthor) {
        authorService.save(apAuthor);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 根据Id查询作者
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/one/{id}")
    public ApAuthor findById(@PathVariable("id") Integer id) {
        return authorService.getById(id);
    }
}
