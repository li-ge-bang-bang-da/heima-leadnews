package com.heima.article.controller.v1;

import com.heima.apis.article.CollectionControllerApi;
import com.heima.article.service.CollectionService;
import com.heima.model.article.dtos.CollectionBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/collection_behavior")
public class CollectionController implements CollectionControllerApi {

    @Autowired
    CollectionService collectionService;
    /**
     * 保存收藏行为
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping
    public ResponseResult collection(@RequestBody CollectionBehaviorDto dto) {
        return collectionService.collection(dto);
    }
}
