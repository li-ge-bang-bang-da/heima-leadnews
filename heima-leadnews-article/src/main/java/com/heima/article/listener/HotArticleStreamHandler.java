package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.heima.article.config.stream.KafkaStreamListener;
import com.heima.model.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.mess.UpdateArticleMess;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Slf4j
public class HotArticleStreamHandler implements KafkaStreamListener<KStream<String,String>> {

    /**
     * 接收消息
     * @return
     */
    @Override
    public String listenerTopic() {
        return HotArticleConstants.HOTARTICLE_SCORE_INPUT_TOPIC;
    }

    /**
     * 流式处理后发送消息
     * @return
     */
    @Override
    public String sendTopic() {
        return HotArticleConstants.HOTARTICLE_INCR_HANDLE_OUPUT_TOPIC;
    }

    @Override
    public KStream<String, String> getService(KStream<String, String> stream) {
        return stream.flatMapValues(new ValueMapper<String, Iterable<? extends String>>() {
            @Override
            public Iterable<? extends String> apply(String value) {
                if (StringUtils.isNotBlank(value)){
                    UpdateArticleMess updateArticleMess = JSON.parseObject(value, UpdateArticleMess.class);
                    log.info("UpdateArticleMess id:{}, type:{}",updateArticleMess.getArticleId(),updateArticleMess.getType());
                    return Arrays.asList(updateArticleMess.getArticleId()+ ":" + updateArticleMess.getType().name());
                }
                return Arrays.asList();
            }
        })
                .groupBy((key,value)->value)
                .windowedBy(TimeWindows.of(5000))
                .count()
                .toStream()
                .map((key,value)->{
                    log.info("处理后的结果key:{}, value:{}",key.key().toString(),value.toString());
                    return new KeyValue<String,String>(key.key().toString(),formatObj(key.key().toString(),value.toString()));
                });
    }

    /**
     *
     * @param key
     * @param value
     * @return
     */
    private String formatObj(String key, String value) {

        ArticleVisitStreamMess mess = new ArticleVisitStreamMess();
        String[] split = key.split(":");
        mess.setArticleId(Long.valueOf(split[0]));

        if (UpdateArticleMess.UpdateArticleType.LIKES.name().equals(split[1])) {
            mess.setLike( Long.valueOf(value) );
        }
        if (UpdateArticleMess.UpdateArticleType.VIEWS.name().equals(split[1])) {
            mess.setView( Long.valueOf(value) );
        }
        if (UpdateArticleMess.UpdateArticleType.COMMENT.name().equals(split[1])) {
            mess.setComment( Long.valueOf(value) );
        }
        if (UpdateArticleMess.UpdateArticleType.COLLECTION.name().equals(split[1])) {
            mess.setCollect( Long.valueOf(value) );
        }

        log.info("formatObj aggs score ArticleVisitStreamMess:{}", mess);

        return JSON.toJSONString(mess);
    }
}
