package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.ApArticleService;
import com.heima.model.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * 实时计算接收的结果
 */
@Component
@Slf4j
public class ArticleIncrHandleListener {

    @Autowired
    ApArticleService apArticleService;

    @KafkaListener(topics = HotArticleConstants.HOTARTICLE_INCR_HANDLE_OUPUT_TOPIC)
    public void receiveMessage(String message) {

        log.info("ArticleIncrHandleListener receiveMessage", message);
        if (StringUtils.isNotBlank(message)){
            ArticleVisitStreamMess articleVisitStreamMess = JSON.parseObject(message, ArticleVisitStreamMess.class);
            apArticleService.updateApArticle(articleVisitStreamMess);
        }
        log.info("************ArticleIncrHandleListener updateApArticle is success************");
    }
}
