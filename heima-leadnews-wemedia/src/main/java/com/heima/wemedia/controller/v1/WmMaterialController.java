package com.heima.wemedia.controller.v1;

import com.heima.apis.wemedia.WmMaterialControllerApi;
import com.heima.model.common.constants.WemediaConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/material")
public class WmMaterialController implements WmMaterialControllerApi {

    @Autowired
    private WmMaterialService wmMaterialService;

    /**
     * 图片上传
     *
     * @param multipartFile
     * @return
     */
    @Override
    @PostMapping("/upload_picture")
    public ResponseResult uploadPicture(@RequestBody MultipartFile multipartFile) {
        return wmMaterialService.uploadPicture(multipartFile);
    }

    /**
     * 查询图片列表
     *
     * @param dto
     * @return
     */
    @Override
    @RequestMapping("/list")
    public ResponseResult findAll(@RequestBody WmMaterialDto dto) {
        return wmMaterialService.findAll(dto);
    }

    /**
     * 删除图片
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/del_picture/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id) {
        return wmMaterialService.delete(id);
    }

    /**
     * 取消收藏
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/cancel_collect/{id}")
    public ResponseResult cancelCollectionMaterial(@PathVariable("id") Integer id) {
        return wmMaterialService.updateStatus(id, WemediaConstants.CANCEL_COLLECT_MATERIAL);
    }

    /**
     * 收藏图片
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/collect/{id}")
    public ResponseResult collectionMaterial(@PathVariable("id") Integer id) {
        return wmMaterialService.updateStatus(id,WemediaConstants.COLLECT_MATERIAL);
    }

}
