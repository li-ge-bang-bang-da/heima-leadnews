package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.exception.CustException;
import com.heima.model.common.constants.AdminConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmUserDto;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.common.AppJwtUtil;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.HashMap;

@Service
public class WmUserServiceImpl extends ServiceImpl<WmUserMapper, WmUser> implements WmUserService {
    /**
     * 自媒体用户登录
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult login(WmUserDto dto) {
        //校验参数
        if (dto == null || StringUtils.isBlank(dto.getName()) ||StringUtils.isBlank(dto.getPassword())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //根据用户名查询用户信息，获取盐
        WmUser wmUser = getOne(Wrappers.<WmUser>lambdaQuery()
                .eq(WmUser::getName, dto.getName()).eq(WmUser::getStatus, AdminConstants.ADUSER_NORMAL_STATUS));

        if (wmUser == null){
            CustException.cust(AppHttpCodeEnum.ADMIN_ADUSER_ISNTOEXIT_ERROR);
        }
        //盐
        String salt = wmUser.getSalt();
        //数据库加密后的密码
        String dbpwd = wmUser.getPassword();


        //按照相同的加密方式加密，比对密码
        String npwd = DigestUtils.md5DigestAsHex((dto.getPassword() + salt).getBytes());
        if (!npwd.equals(dbpwd)){
            CustException.cust(AppHttpCodeEnum.ADMIN_ADUSER_NAMEORPWD_ERROR);
        }

        //更新登录时间
        wmUser.setLoginTime(new Date());
        updateById(wmUser);

        //生成token和用户信息返回给前端
        String token = AppJwtUtil.getToken(wmUser.getId().longValue());

        HashMap<String, Object> result = new HashMap<>();
        result.put("token",token);
        wmUser.setPassword(null);
        wmUser.setSalt(null);
        result.put("user",wmUser);

        return ResponseResult.okResult(result);
    }
}
