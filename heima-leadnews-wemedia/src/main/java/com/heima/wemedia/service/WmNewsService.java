package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

import java.util.List;

public interface WmNewsService extends IService<WmNews> {

    /**
     *  自媒体文章人工审核
     * @param status  2  审核失败  4 审核成功
     * @param dto
     * @return
     */
    public ResponseResult updateStatus(Short status, NewsAuthDto dto);


    /**
     * 查询文章详情
     * @param id
     * @return
     */
    public ResponseResult findWmNewsVo(Integer id) ;


    /**
     * 查询文章列表
     * @param dto
     * @return
     */
    public ResponseResult findList(NewsAuthDto dto);

    /**
     * 查询需要发布的文章id列表
     * @return
     */
    List<Integer> findRelease();


    /**
     * 自媒体文章列表查询
     * @return
     */
    public ResponseResult findAll(WmNewsPageReqDto dto);

    /**
     * 根据id获取文章信息
     * @return
     */
    public ResponseResult findWmNewsById(Integer id);

    /**
     * 删除文章
     * @return
     */
    public ResponseResult delNews(Integer id);

    /**
     * 上下架
     * @param dto
     * @return
     */
    public ResponseResult downOrUp(WmNewsDto dto);


    /**
     * 自媒体文章发布
     * @param dto
     * @param isSubmit  是否为提交 1 为提交 0为草稿
     * @return
     */
    public ResponseResult saveNews(WmNewsDto dto, Short isSubmit);


}
