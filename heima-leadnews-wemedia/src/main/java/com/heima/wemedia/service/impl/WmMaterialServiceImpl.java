package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.exception.CustException;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.threadlocal.WmThreadLocalUtils;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {
    @Autowired
    FileStorageService fileStorageService;

    @Value("${file.oss.profix}")
    private String profix;

    @Value("${file.oss.web-site}")
    private String webSite;

    /**
     * 图片上传
     *
     * @param multipartFile
     * @return
     */
    @Override
    public ResponseResult uploadPicture(MultipartFile multipartFile) {
        //校验参数
        if (multipartFile.isEmpty()) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //获取登录的用户 ID
        WmUser user = WmThreadLocalUtils.getUser();
        if (user == null) {
            CustException.cust(AppHttpCodeEnum.NEED_LOGIN);
        }

        //上传图片
        String originalFilename = multipartFile.getOriginalFilename();  //原文件名称
        String newReplace = UUID.randomUUID().toString().replace("-", "");
        String postfix = originalFilename.substring(originalFilename.lastIndexOf("."));

        String fileId = null;

        try {
            fileId = fileStorageService.store(profix, newReplace + postfix, multipartFile.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            log.error("WmMaterial upload error :{}", e);
            CustException.cust(AppHttpCodeEnum.FILE_UPLOAD_ERROR);
        }

        //文件信息保存到 WmMaterial
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setCreatedTime(new Date());
        wmMaterial.setType((short) 0);
        wmMaterial.setIsCollection((short) 0);
        wmMaterial.setUrl(fileId);
        wmMaterial.setUserId(user.getId());

        save(wmMaterial);

        //前端显示
        if (StringUtils.isBlank(fileId)) {
            CustException.cust(AppHttpCodeEnum.FILE_UPLOAD_ERROR);
        }
        wmMaterial.setUrl(webSite + fileId);

        //返回结果
        return ResponseResult.okResult(wmMaterial);
    }

    /**
     * 查询图片列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findAll(WmMaterialDto dto) {
        //校验参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();

        //执行查询
        //收藏
        Page<WmMaterial> page = new Page<>(dto.getPage(), dto.getSize());
        LambdaQueryWrapper<WmMaterial> queryWrapper = new LambdaQueryWrapper<>();

        if (dto.getIsCollection() != null && dto.getIsCollection() == 1) {
            queryWrapper.eq(WmMaterial::getIsCollection, dto.getIsCollection());
        }

        //当前登录的用户素材
        WmUser user = WmThreadLocalUtils.getUser();
        if (user == null) {
            CustException.cust(AppHttpCodeEnum.NEED_LOGIN);
        }
        queryWrapper.eq(WmMaterial::getUserId, user.getId());

        //按最新时间排序
        queryWrapper.orderByDesc(WmMaterial::getCreatedTime);
        IPage<WmMaterial> result = page(page, queryWrapper);

        //处理图片
        List<WmMaterial> records = result.getRecords();
        for (WmMaterial record : records) {
            record.setUrl(webSite + record.getUrl());
        }

        //返回结果
        return new PageResponseResult(dto.getPage(), dto.getSize(), result.getTotal(), result.getRecords());
    }


    @Autowired
    WmNewsMaterialMapper wmNewsMaterialMapper;

    /**
     * 删除图片
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult delete(Integer id) {
        //参数校验
        if (id == null) {
            CustException.cust(AppHttpCodeEnum.PARAM_INVALID);
        }
        //判断图片是否被引用，被使用则禁止删除
        WmMaterial wmMaterial = getById(id);

        if (wmMaterial == null) {
            CustException.cust(AppHttpCodeEnum.IMAGE_USE_ERROR);
        }
        LambdaQueryWrapper<WmNewsMaterial> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WmNewsMaterial::getMaterialId, id);

        //获取被引用数，如果大于0则删除失败
        Integer count = wmNewsMaterialMapper.selectCount(queryWrapper);
        if (count > 0) {
            CustException.cust(AppHttpCodeEnum.IMAGE_DELETE_ERROR);
        }

        //删除OSS图片
        fileStorageService.delete(wmMaterial.getUrl());

        //删除本地图片
        removeById(id);

        //返回结果

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 收藏与取消收藏
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public ResponseResult updateStatus(Integer id, Integer type) {

        //校验参数
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //修改状态
        Integer uid = WmThreadLocalUtils.getUser().getId();
        update(Wrappers.<WmMaterial>lambdaUpdate().set(WmMaterial::getIsCollection,type)
                        .eq(WmMaterial::getId,id).eq(WmMaterial::getUserId,uid));

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

}
