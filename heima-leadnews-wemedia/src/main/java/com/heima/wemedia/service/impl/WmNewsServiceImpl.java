package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.exception.CustException;
import com.heima.model.common.constants.NewsAutoScanConstants;
import com.heima.model.common.constants.WemediaConstants;
import com.heima.model.common.constants.WmNewsMessageConstants;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.model.wemedia.vos.WmNewsVo;
import com.heima.utils.threadlocal.WmThreadLocalUtils;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmNewsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {

    @Value("${file.oss.web-site}")
    private String webSite;

    @Autowired
    WmNewsMapper wmNewsMapper;

    @Autowired
    WmUserMapper wmUserMapper;

    /**
     * 自媒体文章人工审核
     *
     * @param status 2  审核失败  4 审核成功
     * @param dto
     * @return
     */
    @Override
    public ResponseResult updateStatus(Short status, NewsAuthDto dto) {
        //参数校验
        if (dto == null || dto.getId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询文章
        WmNews wmNews = getById(dto.getId());
        if (wmNews == null || wmNews.getStatus().intValue()==8
                ||wmNews.getStatus().intValue() == 9){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //修改文章状态
        wmNews.setStatus(status);
        if (StringUtils.isNotBlank(dto.getMsg())){
            wmNews.setReason(dto.getMsg());
        }

        updateById(wmNews);
        //返回结果

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 查询文章详情
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult findWmNewsVo(Integer id) {
        //参数校验
        if (id == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询文章详情
        WmNews wmNews = getById(id);
        if (wmNews == null || wmNews.getUserId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //查询作者名称
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());


        //返回结果
        WmNewsVo wmNewsVo = new WmNewsVo();
        BeanUtils.copyProperties(wmNews,wmNewsVo);
        if (wmUser != null){
            wmNewsVo.setAuthorName(wmUser.getName());
        }
        //处理文章封面
        ResponseResult result = ResponseResult.okResult(wmNewsVo);
        result.setHost(webSite);

        return result;
    }

    /**
     * 查询文章列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findList(NewsAuthDto dto) {
        //参数校验
        if (dto == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //设置起始页
        dto.checkParam();
        Integer page = dto.getPage();
        Integer cpage = (page-1)*dto.getSize();
        dto.setPage(cpage);

        //分页查询
        if (StringUtils.isNotBlank(dto.getTitle())){
            dto.setTitle("%"+dto.getTitle()+"%");
        }
        List<WmNewsVo> listAndPage = wmNewsMapper.findListAndPage(dto);

        //统计条数
        long count = wmNewsMapper.findListCount(dto);

        //封装返回结果
        ResponseResult result = new PageResponseResult(page, dto.getSize(),  count,listAndPage);

        return result;
    }

    /**
     * 查询需要发布的文章id列表
     *
     * @return
     */
    @Override
    public List<Integer> findRelease() {
        //查询状态为4或为8的数据且发布时间小于当前时间
        List<WmNews> wmNewsList = list(Wrappers.<WmNews>lambdaQuery()
                .in(WmNews::getStatus, 4, 8)
                .le(WmNews::getPublishTime, new Date())
                .select(WmNews::getId)
        );

        List<Integer> idList = wmNewsList.stream().map(x -> x.getId()).collect(Collectors.toList());

        return idList;
    }

    /**
     * 自媒体文章列表查询
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findAll(WmNewsPageReqDto dto) {
        //校验参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();
        //执行查询
        LambdaQueryWrapper<WmNews> query = new LambdaQueryWrapper<>();
        //状态
        if (dto.getStatus() != null) {
            query.eq(WmNews::getStatus, dto.getStatus());
        }
        //频道ID
        if (dto.getChannelId() != null) {
            query.eq(WmNews::getChannelId, dto.getChannelId());
        }
        //关键字
        if (StringUtils.isNotBlank(dto.getKeyword())) {
            query.like(WmNews::getTitle, dto.getKeyword());
        }
        //发布时间
        if (dto.getBeginPubDate() != null && dto.getEndPubDate() != null) {
            query.between(WmNews::getPublishTime, dto.getBeginPubDate(), dto.getEndPubDate());
        }
        //当前自媒体人的文章
        WmUser user = WmThreadLocalUtils.getUser();
        if (user == null) {
            CustException.cust(AppHttpCodeEnum.NEED_LOGIN);
        }
        query.eq(WmNews::getUserId, user.getId());

        //根据创建时间排序
        query.orderByDesc(WmNews::getCreatedTime);

        IPage<WmNews> page = new Page<>(dto.getPage(), dto.getSize());
        IPage<WmNews> pageResult = page(page, query);


        //返回结果
        PageResponseResult result = new PageResponseResult(dto.getPage(), dto.getSize(), pageResult.getTotal(), pageResult.getRecords());

        //图片返回处理
        result.setHost(webSite);

        return result;
    }


    /**
     * 根据id获取文章信息
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult findWmNewsById(Integer id) {
        //校验参数
        if (id == null) {
            CustException.cust(AppHttpCodeEnum.PARAM_INVALID);
        }

        //执行查询
        WmNews wmNews = getById(id);
        if (wmNews == null) {
            CustException.cust(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //返回结果
        ResponseResult result = ResponseResult.okResult(wmNews);
        result.setHost(webSite);
        return result;
    }

    /**
     * 删除文章
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult delNews(Integer id) {
        //校验参数
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //获取数据
        WmNews wmNews = getById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //判断文章当前的状态,上架且发布则无法删除
        if (wmNews.getStatus().equals(WmNews.Status.PUBLISHED.getCode())
                && wmNews.getEnable().equals(WemediaConstants.WM_NEWS_UPENABLE)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章已发布，不能删除");
        }
        //删除素材与文章的关联
        wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery()
                .eq(WmNewsMaterial::getNewsId, wmNews.getId()));

        removeById(wmNews.getId());
        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 上下架
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult downOrUp(WmNewsDto dto) {
        //校验参数
        if (dto == null || dto.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //查询文章
        WmNews wmNews = getById(dto.getId());
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章不存在");
        }

        //判断文章的状态
        if (!wmNews.getStatus().equals(WemediaConstants.WM_NEWS_PUBLISH_STATUS)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "当前文章不是发布状态，不能上下架");
        }


        //执行修改
        wmNews.setEnable(dto.getEnable());
        updateById(wmNews);
        //同步到APP端
        if (wmNews.getArticleId()!= null){
            Map<String,Object> map = new HashMap<>();
            map.put("enable",dto.getEnable());
            map.put("articleId",wmNews.getArticleId());
            kafkaTemplate.send(WmNewsMessageConstants.WM_NEWS_UP_OR_DOWN_TOPIC,JSON.toJSONString(map));
        }

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    @Autowired
    WmNewsMaterialMapper wmNewsMaterialMapper;

    @Autowired
    WmMaterialMapper wmMaterialMapper;

    @Autowired
    KafkaTemplate kafkaTemplate;

    /**
     * 自媒体文章发布
     *
     * @param dto
     * @param isSubmit 是否为提交 1 为提交 0为草稿
     * @return
     */
    @Override
    @Transactional
    public ResponseResult saveNews(WmNewsDto dto, Short isSubmit) {
        //校验参数
        if (dto == null || StringUtils.isBlank(dto.getContent())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //保存或修改文章
        WmNews wmNews = new WmNews();
        //把dto的参数copy到wmNews中
        BeanUtils.copyProperties(dto, wmNews);
        //如果文章图片是自动的，那就先设为null
        if (dto.getType().equals(WemediaConstants.WM_NEWS_TYPE_AUTO)) {
            wmNews.setType(null);
        }

        //将文章封面的集合转换成字符串
        String images = imageListToStr(dto.getImages(), webSite);
        if (StringUtils.isNotBlank(images)) {
            wmNews.setImages(images);
        }
        saveWmNews(wmNews,isSubmit);

        //关联文章和素材的关系（idSubmit=1 提交即关联）
        List<Map> contentList = JSON.parseArray(dto.getContent(), Map.class);
        //解析内容中的图片列表
        List<String> materials = parseContentImages(contentList);

        //关联文章内容和素材的关系
        if (isSubmit == WmNews.Status.SUBMIT.getCode() && materials.size() > 0) {
            saveRelativeInfoForContent(materials, wmNews.getId());
        }

        //关联文章封面和素材的关系
        if (isSubmit == WmNews.Status.SUBMIT.getCode()) {
            saveRelativeInfoForCover(dto, materials, wmNews);
        }


        // ******发消息给admin 审核
        Map<String,Integer> map = new HashMap<>();
        map.put("wmNewsId",wmNews.getId());
        kafkaTemplate.send(NewsAutoScanConstants.WM_NEWS_AUTO_SCAN_TOPIC,JSON.toJSONString(map));

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    /**
     * 关联文章封面和素材的关系
     *
     * @param dto       前端参数
     * @param materials 文章内容中的图片列表
     * @param wmNews
     */
    private void saveRelativeInfoForCover(WmNewsDto dto, List<String> materials, WmNews wmNews) {
        //获取封面图片
        List<String> images = dto.getImages();
        //判断type是否为自动
        if (dto.getType().equals(WemediaConstants.WM_NEWS_TYPE_AUTO)) {
            int size = materials.size();
            if (size > 0 && size <= 2) { //单图
                images = materials.stream().limit(1).collect(Collectors.toList());
                wmNews.setType(WemediaConstants.WM_NEWS_SINGLE_IMAGE);
            } else if (size > 2) {  //多图
                images = materials.stream().limit(3).collect(Collectors.toList());
                wmNews.setType(WemediaConstants.WM_NEWS_MANY_IMAGE);
            } else {     //无图
                wmNews.setType(WemediaConstants.WM_NEWS_NONE_IMAGE);
            }

            if (images != null) {
                wmNews.setImages(imageListToStr(images, webSite));
            }

            updateById(wmNews);
        }

        //保存
        if (images != null && images.size() > 0) {
            saveRelativeInfoForImage(images, wmNews.getId());
        }

    }

    /**
     * 保存文章封面和图片的关系
     *
     * @param images
     * @param newsId
     */
    private void saveRelativeInfoForImage(List<String> images, Integer newsId) {
        images = images.stream()
                .map(x -> x.replace(webSite, "")
                        .replace(" ", ""))
                .collect(Collectors.toList());

        saveRelativeInfo(images, newsId, WemediaConstants.WM_IMAGE_REFERENCE);
    }

    /**
     * 关联文章内容和素材的关系
     *
     * @param materials
     * @param newsId
     */
    private void saveRelativeInfoForContent(List<String> materials, Integer newsId) {
        saveRelativeInfo(materials, newsId, WemediaConstants.WM_CONTENT_REFERENCE);
    }

    /**
     * 保存文章与素材的关系
     *
     * @param materials
     * @param newsId
     * @param type
     */
    private void saveRelativeInfo(List<String> materials, Integer newsId, Short type) {
        //查询文章中图片对应的素材ID
        List<WmMaterial> dbmaterials1 = wmMaterialMapper.selectList(Wrappers.<WmMaterial>lambdaQuery()
                .in(WmMaterial::getUrl, materials)
                .eq(WmMaterial::getUserId, WmThreadLocalUtils.getUser().getId())
        );

        //校验素材列表
        if (dbmaterials1 == null || dbmaterials1.size() == 0) {
            CustException.cust(AppHttpCodeEnum.FILE_MATERIAL_ISNULL_ERROR);
        }

        //判断文章素材是否存在
        Map<String, Integer> materials1Map = dbmaterials1.stream()
                .collect(Collectors.toMap(WmMaterial::getUrl, WmMaterial::getId));

        List<Integer> materialIds = new ArrayList<>();
        for (String url : materials) {
            Integer id = materials1Map.get(url);
            if (id == null) {
                CustException.cust(AppHttpCodeEnum.FILE_MATERIAL_ISNULL_ERROR);
            }
            materialIds.add(id);
        }

        //保存素材关系
        wmNewsMaterialMapper.saveRelations(materialIds, newsId, type);

    }

    /**
     * 解析内容中的图片列表
     *
     * @param contentList
     * @return
     */
    private List<String> parseContentImages(List<Map> contentList) {
        ArrayList<String> materials = new ArrayList<>();
        for (Map<String, String> content : contentList) {
            if (content.get("type").equals(WemediaConstants.WM_NEWS_TYPE_IMAGE)) {
                String imageUrl = content.get("value")
                        .replace(webSite, "")
                        .replace(" ", "");
                materials.add(imageUrl);
            }
        }

        return materials;
    }

    /**
     * 保存或修改文章
     *
     * @param wmNews
     * @param isSubmit
     */
    private void saveWmNews(WmNews wmNews, Short isSubmit) {

        wmNews.setCreatedTime(new Date());
        wmNews.setUserId(WmThreadLocalUtils.getUser().getId());
        wmNews.setSubmitedTime(new Date());
        wmNews.setEnable(WemediaConstants.WM_NEWS_UPENABLE);

        if (wmNews.getId() == null) {
            save(wmNews);       //保存
        } else {
            wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery()
                    .eq(WmNewsMaterial::getNewsId, wmNews.getId()));
            updateById(wmNews); //当前文章和素材库的关系表数据先删除 后修改
        }
    }

    /**
     * 把image图片列表转换成字符串，并且去除前缀
     *
     * @param images
     * @param webSite
     * @return
     */
    private String imageListToStr(List<String> images, String webSite) {

        if (images.size() > 0) {
            return images.toString()
                    .replace("[", "")
                    .replace("]", "")
                    .replace(webSite, "")
                    .replace(" ", "");
        }

        return null;
    }

}

