package com.heima.comment.service.impl;

import com.heima.comment.pojos.ApComment;
import com.heima.comment.service.CommentHotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentHotServiceImpl implements CommentHotService {

    @Autowired
    MongoTemplate mongoTemplate;

    /**
     * 处理热点评论
     *
     * @param entryId   文章ID
     * @param apComment 评论对象
     */
    @Override
    @Async   //开启异步执行
    public void hotCommentExecutor(Long entryId, ApComment apComment) {

        //1. 查询当前文章热点评论的所有数据，按照点赞数倒序
        List<ApComment> hotCommentList = mongoTemplate.find(Query.query(Criteria
                        .where("entryId").is(entryId)
                        .and("type").is(0)
                        .and("flag").is(1)
                ).with(Sort.by(Sort.Order.desc("likes"))),
                ApComment.class);
        //2. 如果当前没有热点数据，直接新增
        if (hotCommentList == null || hotCommentList.size() < 5 ) {
            apComment.setFlag((short) 1);
            mongoTemplate.save(apComment);
            return;
        }
        //3. 如果当前有热点数据，则判断当前评论的点赞数是否大于热点评论中的最小点赞数
        ApComment hotcomment = hotCommentList.get(hotCommentList.size() - 1);  //获取当前热点评论的最后一条
        if (apComment.getLikes().intValue() > hotcomment.getLikes().intValue()) {
            //4. 如果当前评论数据大于5条，则替换热点评论中的最后一条数据
            if (hotCommentList.size() > 5){     //热点评论是否大于5条
                hotcomment.setFlag((short) 0);      //大于5条则置换最后一条
                mongoTemplate.save(hotcomment);

                //把当前的评论置为热点评论
//                apComment.setFlag((short) 1);
//                mongoTemplate.save(apComment);
            }
            //5. 如果当前热点评论不满足5条，则直接新增
            apComment.setFlag((short) 1);
            mongoTemplate.save(apComment);
        }
    }
}
