package com.heima.comment.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.aliyun.GreeTextScan;
import com.heima.comment.pojos.ApComment;
import com.heima.comment.pojos.ApCommentLike;
import com.heima.comment.service.CommentHotService;
import com.heima.comment.service.CommentService;
import com.heima.comment.vos.ApCommentVo;
import com.heima.feigns.user.UserFeign;
import com.heima.model.comment.CommentDto;
import com.heima.model.comment.CommentLikeDto;
import com.heima.model.comment.CommentSaveDto;
import com.heima.model.common.constants.HotArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommentServiceImpl implements CommentService {
    @Autowired
    UserFeign userFeign;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    GreeTextScan greeTextScan;

    @Autowired
    KafkaTemplate kafkaTemplate;

    /**
     * 根据文章id查询评论列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findByArticleId(CommentDto dto) {
        //参数校验
        if (dto == null || dto.getArticleId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        Integer size = dto.getSize();
        if (size == null || size <= 0) {
            dto.setSize(10);
        }

        List<ApComment> commentList = null;
        //判断是否是首页 ，如果是首页则查询热点评论
        if (dto.getIndex().intValue() == 1) {
            //查询所有的热点评论
            commentList = mongoTemplate.find(Query.query(Criteria
                            .where("entryId").is(dto.getArticleId())
                            .and("type").is(0)
                            .and("flag").is(1)
                    ).with(Sort.by(Sort.Order.desc("likes"))),
                    ApComment.class);
            //查询普通评论
            List<ApComment> apComments = mongoTemplate.find(Query.query(Criteria
                            .where("entryId").is(dto.getArticleId())
                            .and("createdTime").lt(dto.getMinDate())
                            .and("type").is(0)
                            .and("flag").is(0))
                            .limit(dto.getSize() - commentList.size())
                            .with(Sort.by(Sort.Order.desc("createdTime"))),
                    ApComment.class);
            //合并评论列表
            commentList.addAll(apComments);

        } else {
            //查询文章的所有评论
            commentList = mongoTemplate.find(Query.query(Criteria
                            .where("entryId").is(dto.getArticleId())
                            .and("createdTime").lt(dto.getMinDate()))
                            .limit(dto.getSize())
                            .with(Sort.by(Sort.Order.desc("createdTime"))),
                    ApComment.class);
        }

        //3 封装查询结果
        //3.1 用户未登录 直接返回评论列表
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.okResult(commentList);
        }

        //3.2 用户登录，需要回显当前用户对评论点赞的列表
        List<ApCommentVo> commentVoListVo = new ArrayList<>();

        // 获取文章对应的所有评论ID列表
        List<String> idList = commentList.stream().map(x -> x.getId()).collect(Collectors.toList());

        // 查询评论点赞列表 按照评论id 筛选
        List<ApCommentLike> commentLikes = mongoTemplate.find(Query.query(Criteria
                        .where("authorId").is(user.getId())
                        .and("commentId").in(idList)
                        .and("operation").is(0)),
                ApCommentLike.class);

        // 遍历当前文章的评论列表
        if (commentList != null && commentList.size() > 0 && commentLikes != null
                && commentLikes.size() > 0
        ) {
            commentList.forEach(apComment -> {
                ApCommentVo apCommentVo = new ApCommentVo();
                BeanUtils.copyProperties(apComment, apCommentVo);

                //处理回显 遍历当前用户点赞列表
                commentLikes.forEach(apCommentLike -> {
                    //当前用户对这条评论的点赞
                    if (apComment.getId().equals(apCommentLike.getCommentId())) {
                        apCommentVo.setOperation((short) 0);
                    }

                });
                commentVoListVo.add(apCommentVo);

            });
            return ResponseResult.okResult(commentVoListVo);
        }
        //返回结果
        return ResponseResult.okResult(commentList);
    }

    @Autowired
    CommentHotService commentHotService;

    /**
     * 点赞某一条评论
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult like(CommentLikeDto dto) {
        //参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询当前登录的用户
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //查询用户信息
        ApUser apUser = userFeign.findUserById(user.getId());
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "非法用户");
        }
        //查询是否有评论
        ApComment apComment = mongoTemplate.findById(dto.getCommentId(), ApComment.class);
        if (apComment == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "暂无评论");
        }

        //查询是否重复点赞
        ApCommentLike commentLike = mongoTemplate.findOne(Query.query(
                Criteria.where("commentId").is(dto.getCommentId())
                        .and("authorId").is(user.getId())),
                ApCommentLike.class);
        if (commentLike != null && commentLike.getOperation() == 0 && dto.getOperation() == 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "请勿重复点赞");
        }
        //保存点赞  TODO：分布式锁的BUG 就业课补全
        if (dto.getOperation().intValue() == 0) {
            //第一次点赞
            if (commentLike == null) {
                commentLike = new ApCommentLike();
                commentLike.setCommentId(apComment.getId());
                commentLike.setAuthorId(user.getId());
            }
            //更新点赞
            commentLike.setOperation(dto.getOperation());
            mongoTemplate.save(commentLike);

            //点赞数 + 1
            apComment.setLikes(apComment.getLikes() + 1);
            mongoTemplate.save(apComment);


            // TODO: 计算热点评论  异步调用
            if (apComment.getLikes() >= 5 && apComment.getFlag().shortValue() == 0) {
                commentHotService.hotCommentExecutor(apComment.getEntryId(), apComment);
            }


        } else {     //取消点赞
            commentLike.setOperation((short) 1);
            mongoTemplate.save(commentLike);

            //点赞数 - 1
            apComment.setLikes((apComment.getLikes() - 1) < 0 ? 0 : apComment.getLikes() - 1);
            mongoTemplate.save(apComment);

        }

        //返回结果
        Map<String, Integer> resultMap = new HashMap<>();
        resultMap.put("likes", apComment.getLikes());
        return ResponseResult.okResult(resultMap);
    }

    /**
     * 保存评论
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult saveComment(CommentSaveDto dto) {
        //参数校验
        if (dto == null || dto.getArticleId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        if (dto.getContent() == null || dto.getContent().length() > 140) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "评论内容不能为空或者字数超过140");
        }
        //获取当前登录的用户
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //TODO : 阿里云接口校验  待修改
        try {
            Map map = greeTextScan.greeTextScan("content");
            if (map.get("suggestion").equals("block")) {
                return ResponseResult.okResult(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("评论内容非法，请重新输入, e:{}", e);
        }

        //保存评论
        ApUser apUser = userFeign.findUserById(user.getId());
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        ApComment apComment = new ApComment();
        apComment.setAuthorId(apUser.getId());
        apComment.setAuthorName(apUser.getName());
        apComment.setImage(apUser.getImage());
        apComment.setContent(dto.getContent());
        apComment.setCreatedTime(new Date());
        apComment.setUpdatedTime(new Date());
        apComment.setEntryId(dto.getArticleId());
        apComment.setLikes(0);
        apComment.setReply(0);
        apComment.setType((short) 0);
        apComment.setFlag((short) 0);
        mongoTemplate.insert(apComment);

        //发消息给kafkaStream 实时计算
        UpdateArticleMess updateArticleMess = new UpdateArticleMess();
        updateArticleMess.setType(UpdateArticleMess.UpdateArticleType.COMMENT);
        updateArticleMess.setArticleId(dto.getArticleId());
        kafkaTemplate.send(HotArticleConstants.HOTARTICLE_SCORE_INPUT_TOPIC,
                UUID.randomUUID().toString(), JSON.toJSONString(updateArticleMess));


        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
