package com.heima.comment.service.impl;

import com.heima.comment.pojos.ApComment;
import com.heima.comment.pojos.ApCommentRepay;
import com.heima.comment.pojos.ApCommentRepayLike;
import com.heima.comment.service.CommentRepayService;
import com.heima.comment.vos.ApCommentRepayVo;
import com.heima.feigns.user.UserFeign;
import com.heima.model.comment.CommentRepayDto;
import com.heima.model.comment.CommentRepayLikeDto;
import com.heima.model.comment.CommentRepaySaveDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CommentRepayServiceimpl implements CommentRepayService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    UserFeign userFeign;

    /**
     * 查看更多回复内容
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult loadCommentRepay(CommentRepayDto dto) {
        //参数校验
        if (dto == null || dto.getCommentId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        Integer size = dto.getSize();
        if (size == null || size <= 0) {
            dto.setSize(10);
        }
        //查询评论的所有回复
        List<ApCommentRepay> commentRepayList = mongoTemplate.find(Query.query(Criteria
                        .where("commentId").is(dto.getCommentId())
                        .and("createdTime").lt(dto.getMinDate()))
                        .limit(dto.getSize())
                        .with(Sort.by(Sort.Order.desc("createdTime"))),
                ApCommentRepay.class);
        //3 封装查询结果
        //3.1 用户未登录 直接返回评论列表
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.okResult(commentRepayList);
        }

        //3.2 用户登录，需要回显当前用户对评论点赞的列表
        List<ApCommentRepayVo> commentRepayVoList = new ArrayList<>();

        // 获取评论回复对应的所有评论ID列表
        List<String> idList = commentRepayList.stream().map(x -> x.getId()).collect(Collectors.toList());

        // 查询回复评论点赞列表 按照评论id 筛选
        List<ApCommentRepayLike> commentRepayLikes = mongoTemplate.find(Query.query(Criteria
                        .where("authorId").is(user.getId())
                        .and("commentRepayId").in(idList)
                        .and("operation").is(0)),
                ApCommentRepayLike.class);

        // 遍历当前回复的评论列表
        if (commentRepayList != null && commentRepayList.size() > 0 && commentRepayLikes != null
                && commentRepayLikes.size() > 0
        ) {
            commentRepayList.forEach(apCommentRepay -> {
                ApCommentRepayVo apCommentRepayVo = new ApCommentRepayVo();
                BeanUtils.copyProperties(apCommentRepay, apCommentRepayVo);

                //处理回显 遍历当前用户点赞列表
                commentRepayLikes.forEach(apCommentRepayLike -> {
                    //当前用户对这条评论的点赞
                    if (apCommentRepay.getId().equals(apCommentRepayLike.getCommentRepayId())) {
                        apCommentRepayVo.setOperation((short) 0);
                    }

                });
                commentRepayVoList.add(apCommentRepayVo);

            });
            return ResponseResult.okResult(commentRepayVoList);
        }

        //返回结果
        return ResponseResult.okResult(commentRepayList);
    }

    /**
     * 保存回复
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult saveCommentRepay(CommentRepaySaveDto dto) {
        //参数校验
        if (dto == null || dto.getCommentId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        if (dto.getContent() == null || dto.getContent().length() > 140) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "回复内容不能为空或者字数超过140");
        }
        //获取当前登录的用户
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //TODO : 阿里云接口校验  待修改

        //保存回复评论
        ApUser apUser = userFeign.findUserById(user.getId());
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        ApCommentRepay apCommentRepay = new ApCommentRepay();
        apCommentRepay.setAuthorId(apUser.getId());
        apCommentRepay.setAuthorName(apUser.getName());
        apCommentRepay.setContent(dto.getContent());
        apCommentRepay.setCommentId(dto.getCommentId());
        apCommentRepay.setCreatedTime(new Date());
        apCommentRepay.setUpdatedTime(new Date());
        apCommentRepay.setLikes(0);
        mongoTemplate.insert(apCommentRepay);

        //保存回复的次数
        ApComment apComment = mongoTemplate.findById(dto.getCommentId(), ApComment.class);
        apComment.setReply(apComment.getReply() + 1);
        mongoTemplate.save(apComment);

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 点赞回复的评论
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult saveCommentRepayLike(CommentRepayLikeDto dto) {
        //参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询当前登录的用户
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //查询用户信息
        ApUser apUser = userFeign.findUserById(user.getId());
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "非法用户");
        }
        //查询是否有回复评论
        ApCommentRepay apCommentRepay = mongoTemplate.findById(dto.getCommentRepayId(), ApCommentRepay.class);
        if (apCommentRepay == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "暂无评论");
        }

        //查询是否重复点赞
        ApCommentRepayLike commentRepayLike = mongoTemplate.findOne(Query.query(
                Criteria.where("commentRepayId").is(dto.getCommentRepayId())
                        .and("authorId").is(user.getId())),
                ApCommentRepayLike.class);
        if (commentRepayLike!= null && dto.getOperation().intValue() == 0
                &&commentRepayLike.getOperation().intValue() == 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "请勿重复点赞");
        }
        //保存点赞  TODO：分布式锁的BUG 就业课补全

        if (dto.getOperation().intValue() == 0) {
            //第一次点赞
            if (commentRepayLike == null) {
                commentRepayLike = new ApCommentRepayLike();
                commentRepayLike.setCommentRepayId(apCommentRepay.getId());
                commentRepayLike.setAuthorId(user.getId());
            }
            //更新点赞
            commentRepayLike.setOperation(dto.getOperation());
            mongoTemplate.save(commentRepayLike);

            //点赞数 + 1
            apCommentRepay.setLikes(apCommentRepay.getLikes() + 1);
            mongoTemplate.save(apCommentRepay);

        } else {     //取消点赞
            commentRepayLike.setOperation((short) 1);
            mongoTemplate.save(commentRepayLike);

            //点赞数 - 1
            apCommentRepay.setLikes((apCommentRepay.getLikes() - 1) < 0 ? 0 : apCommentRepay.getLikes() - 1);
            mongoTemplate.save(apCommentRepay);

        }

        //返回结果
        Map<String, Integer> resultMap = new HashMap<>();
        resultMap.put("likes", apCommentRepay.getLikes());
        return ResponseResult.okResult(resultMap);

    }
}
