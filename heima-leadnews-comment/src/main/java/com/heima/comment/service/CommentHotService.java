package com.heima.comment.service;

import com.heima.comment.pojos.ApComment;

public interface CommentHotService {

    /**
     * 处理热点评论
     * @param entryId      文章ID
     * @param apComment     评论对象
     */
    public void hotCommentExecutor(Long entryId, ApComment apComment);
}
