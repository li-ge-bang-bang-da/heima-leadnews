package com.heima.comment.service;

import com.heima.model.comment.CommentRepayDto;
import com.heima.model.comment.CommentRepayLikeDto;
import com.heima.model.comment.CommentRepaySaveDto;
import com.heima.model.common.dtos.ResponseResult;

/**
 * 评论回复操作
 */
public interface CommentRepayService {

    /**
     * 查看更多回复内容
     * @param dto
     * @return
     */
    public ResponseResult loadCommentRepay(CommentRepayDto dto);

    /**
     * 保存回复
     * @return
     */
    public ResponseResult saveCommentRepay(CommentRepaySaveDto dto);

    /**
     * 点赞回复的评论
     * @param dto
     * @return
     */
    public ResponseResult saveCommentRepayLike(CommentRepayLikeDto dto);
}
