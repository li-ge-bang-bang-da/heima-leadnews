package com.heima.common.exception;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Configuration
@Slf4j
@RestControllerAdvice
public class ExceptionCatch {
    /**
     * 处理项目中所有的异常拦截
     * @param ex
     * @return
     */

    //不可预知异常

    @ExceptionHandler(Exception.class)
    public ResponseResult exception(Exception ex){

        //记录日志
        log.error("ExceptionCatch ex:{}",ex);

        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR, "您的网络异常，请稍后重试");

    }

    /**
     * 可预知异常
     * @param ex
     * @return
     */
    @ExceptionHandler(CustomException.class)
    public ResponseResult custException(CustomException ex){
        //记录日志
        log.error("CustomException ex:{}",ex);

        ex.printStackTrace();//打印

        return ResponseResult.errorResult(ex.getAppHttpCodeEnum());
    }



}
