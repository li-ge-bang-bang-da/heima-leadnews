package com.heima.common.exception;

import com.heima.model.common.enums.AppHttpCodeEnum;

/**
 * 抛异常工具类
 */
public class CustException {

    public static void cust(AppHttpCodeEnum appHttpCodeEnum){
        throw new CustomException(appHttpCodeEnum);
    }
}