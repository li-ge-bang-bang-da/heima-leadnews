package com.heima.common.exception;

import com.heima.model.common.enums.AppHttpCodeEnum;

public class CustomException extends RuntimeException {

    //异常处理的枚举
    private AppHttpCodeEnum appHttpCodeEnum;

    public AppHttpCodeEnum getAppHttpCodeEnum() {
        return appHttpCodeEnum;
    }


    public CustomException(AppHttpCodeEnum appHttpCodeEnum) {
        this.appHttpCodeEnum = appHttpCodeEnum;
    }
}
