package com.heima.search.listener;

import com.alibaba.fastjson.JSON;
import com.heima.model.common.constants.ArticleConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
@Slf4j
public class SyncArticleListener {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @KafkaListener(topics = ArticleConstants.ARTICLE_ES_SYNC_TOPIC)
    public void recevieMessage(String message) {

        log.info("SyncArticleListener recevieMessage :{}", message);
        if (StringUtils.isNotBlank(message)){
            Map map = JSON.parseObject(message, Map.class);
            IndexRequest indexRequest = new IndexRequest("app_info_article");
            indexRequest.id(map.get("id").toString());
            indexRequest.source(message, XContentType.JSON);

            try {
                restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
                log.error("sync es error:{}", e);
            }
        }

        log.info("*******************增量文章到索引库成功*******************");
    }

}
