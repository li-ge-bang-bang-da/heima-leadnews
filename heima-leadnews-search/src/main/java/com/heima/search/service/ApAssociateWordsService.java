package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

public interface ApAssociateWordsService {

    /**
     联想词
     @param dto
     @return
     */
    ResponseResult findAssociate(UserSearchDto dto);
}
