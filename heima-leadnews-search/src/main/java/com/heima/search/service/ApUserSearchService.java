package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.HistorySearchDto;
import com.heima.model.search.dtos.UserSearchDto;

public interface ApUserSearchService {

    /**
     * 保存用户搜索历史记录
     * @param dto
     */
    public void insert( UserSearchDto dto);

    /**
     * 查询搜索历史
     * @param dto
     * @return
     */
    public ResponseResult findUserSearch(UserSearchDto dto);

    /**
     删除搜索历史
     @param dto
     @return
     */
    ResponseResult delUserSearch(HistorySearchDto dto);
}
