package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ApArticleSearchService;
import com.heima.search.service.ApUserSearchService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ApArticleSearchServiceImpl implements ApArticleSearchService {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Value("${file.oss.web-site}")
    private String webSite;

    @Autowired
    ApUserSearchService apUserSearchService;

    /**
     * 搜索文章
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult search(UserSearchDto dto) {
        //参数校验
        if (dto == null || StringUtils.isBlank(dto.getSearchWords())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"抱歉，找不到您的需求！");
        }

        //******异步调用保存用户搜索关键词
        apUserSearchService.insert(dto);

        //构建查询
        BoolQueryBuilder queryBuild = QueryBuilders.boolQuery();
        SearchSourceBuilder sourceBuild = new SearchSourceBuilder();

        //关键词查询
        MatchQueryBuilder titleQueryBuilder = QueryBuilders.matchQuery("title", dto.getSearchWords()).analyzer("ik_smart");
        queryBuild.must(titleQueryBuilder);

        //发布时间
        if (dto.getMinBehotTime() == null) {
            dto.setMinBehotTime(new Date());
        }
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("publishTime").lte(dto.getMinBehotTime());
        queryBuild.filter(rangeQueryBuilder);

        //分页
        if (dto.getPageSize() <= 0 || dto.getPageSize() > 50) {
            dto.setPageSize(10);
        }
        sourceBuild.from(0);
        sourceBuild.size(dto.getPageSize());

        //按照发布时间的倒序排序
        sourceBuild.sort("publishTime", SortOrder.DESC);

        //高亮 显示
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("title");
        highlightBuilder.preTags("<font style='color:red'>");
        highlightBuilder.postTags("</font>");
        sourceBuild.highlighter(highlightBuilder);

        //执行查询
        SearchRequest searchRequest = new SearchRequest("app_info_article");
        sourceBuild.query(queryBuild);
        searchRequest.source(sourceBuild);
        //解析结果并封装
        List<Map> resultList = new ArrayList<>();
        try {
            SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            SearchHit[] searchHits = hits.getHits();
            for (SearchHit searchHit : searchHits) {

                Map<String, Object> map = searchHit.getSourceAsMap();
                //获取高亮结果集
                Text[] titles = searchHit.getHighlightFields().get("title").getFragments();
                //保留原始标题
                map.put("o_title", map.get("title"));
                //设置文章id,解决精度问题
                map.put("id",map.get("id").toString());
                //解析高亮
                String title = StringUtils.join(titles);
                if (titles !=null && titles.length > 0) {
                    map.put("title", title);
                }

                resultList.add(map);
            }

        } catch (IOException e) {
            e.printStackTrace();
            log.info("search result error :{}", e);
        }


        //处理图片 返回结果
        ResponseResult result = ResponseResult.okResult(resultList);
        result.setHost(webSite);
        return result;
    }
}
