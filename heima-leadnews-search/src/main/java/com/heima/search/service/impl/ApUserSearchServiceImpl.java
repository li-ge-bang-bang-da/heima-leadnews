package com.heima.search.service.impl;

import com.heima.feigns.behavior.BehaviorFeign;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.HistorySearchDto;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.search.pojos.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import com.mongodb.client.result.DeleteResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ApUserSearchServiceImpl implements ApUserSearchService {

    @Autowired
    BehaviorFeign behaviorFeign;

    @Autowired
    MongoTemplate mongoTemplate;


    /**
     * 删除搜索历史
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult delUserSearch(HistorySearchDto dto) {
        //参数校验
        if (dto.getEquipmentId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //查询登录用户
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //查询行为实体
        ApBehaviorEntry behaviorEntry = behaviorFeign.findByUserIdOrEquipmentId(user.getId(), dto.getEquipmentId());
        if (behaviorEntry == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //执行删除
        Query query = Query.query(Criteria
                .where("entryId").is(behaviorEntry.getId())
                .and("id").is(dto.getHistoryId())
        );
        mongoTemplate.remove(query, ApUserSearch.class);

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 查询搜索历史
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findUserSearch(UserSearchDto dto) {
        //参数校验
        if (dto == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //获取登录用户
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //查询行为实体
        ApBehaviorEntry behaviorEntry = behaviorFeign.findByUserIdOrEquipmentId(user.getId(), dto.getEquipmentId());
        if (behaviorEntry == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //查询当前用户的搜索列表  按照时间倒序 .limit(userSearchDto.getPageSize())
        List<ApUserSearch> userSearchList = mongoTemplate.find(Query.query(Criteria
                        .where("entryId").is(behaviorEntry.getId())
                ).with(Sort.by(Sort.Order.desc("createdTime"))),
                ApUserSearch.class);

        return ResponseResult.okResult(userSearchList);
    }

    /**
     * 保存用户搜索历史记录
     *
     * @param dto
     */
    @Override
    @Async
    public void insert(UserSearchDto dto) {
        //参数校验
        if (dto == null|| StringUtils.isBlank(dto.getSearchWords())){
                return;
        }

        ApUser user = AppThreadLocalUtils.getUser();
        if (user != null){
            //查询行为实体
            ApBehaviorEntry behaviorEntry = behaviorFeign.findByUserIdOrEquipmentId(user.getId(), dto.getEquipmentId());
            if (behaviorEntry!=null){
                // 查询当前用户搜索的关键词是否存在
                ApUserSearch userSearch = mongoTemplate.findOne(Query.query(Criteria
                                .where("keyword").is(dto.getSearchWords())
                                .and("entryId").is(behaviorEntry.getId())),
                        ApUserSearch.class);

                //查询当前用户的关键词列表
                List<ApUserSearch> userSearchList = mongoTemplate.find(Query.query(Criteria
                                .where("entryId").is(behaviorEntry.getId())
                        ).with(Sort.by(Sort.Order.desc("createdTime"))),
                        ApUserSearch.class);

                //存在则更新最新时间，更新状态
                if (userSearch !=null){
                    userSearch.setCreatedTime(new Date());
                    mongoTemplate.save(userSearch);
                    return;
                }
                //不存在则新增, 判断当前历史记录是否超过10条记录
                ApUserSearch apUserSearch = new ApUserSearch();
                apUserSearch.setCreatedTime(new Date());
                apUserSearch.setEntryId(behaviorEntry.getId());
                apUserSearch.setKeyword(dto.getSearchWords());
                if (userSearchList == null || userSearchList.size()<10){
                    mongoTemplate.save(apUserSearch);
                }else {
                    ApUserSearch lastsearch = userSearchList.get(userSearchList.size() - 1); //获取最后一条记录
                    Query query = Query.query(Criteria.where("id").is(lastsearch.getId()));
                    mongoTemplate.findAndReplace(query,apUserSearch);
                }
            }

        }
    }
}
