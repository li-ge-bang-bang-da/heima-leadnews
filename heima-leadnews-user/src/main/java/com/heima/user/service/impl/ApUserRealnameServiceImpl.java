package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.exception.CustException;
import com.heima.feigns.article.ArticleFeign;
import com.heima.feigns.wemedia.WemediaFeign;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.constants.AdminConstants;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.AuthDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserRealname;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.mapper.ApUserRealnameMapper;
import com.heima.user.service.ApUserRealnameService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ApUserRealnameServiceImpl extends ServiceImpl<ApUserRealnameMapper, ApUserRealname> implements ApUserRealnameService {

    @Autowired
    ApUserMapper apUserMapper;

    @Autowired
    WemediaFeign wemediaFeign;

    @Autowired
    ArticleFeign articleFeign;

    /**
     * 根据状态进行审核
     *
     * @param dto
     * @param status 2 审核失败   9 审核成功
     * @return
     */
    @Override
    @GlobalTransactional(rollbackFor = Exception.class)  //开启分布式事务管理

    public ResponseResult updateStatusById(AuthDto dto, Short status) {
        //检查参数
        if (dto == null || dto.getId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //获取ApUserRealname
        ApUserRealname userRealname = getById(dto.getId());
        if (userRealname == null || userRealname.getUserId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //获取apUser id
        ApUser apUser = apUserMapper.selectById(userRealname.getUserId());
        if (apUser == null || apUser.getStatus()){
            return ResponseResult.errorResult(AppHttpCodeEnum.APUSER_STATUS_ERROR);
        }

        //修改状态
        userRealname.setStatus(status);
        userRealname.setUpdatedTime(new Date());
            //如果驳回返回原因
        if (StringUtils.isNotBlank(dto.getMsg())){
            userRealname.setReason(dto.getMsg());
        }
        updateById(userRealname);


        //如果审核通过
        if (AdminConstants.PASS_AUTH.intValue() == status.intValue()){

            //远程创建自媒体账户
            WmUser wmUser = createWmUser(dto,apUser);

            if (wmUser == null || wmUser.getId() == null){
                CustException.cust(AppHttpCodeEnum.WMUSER_ADD_ERROR);
            }

            //远程创建作者账户
            boolean flag = createApAuthor(wmUser);

            if (!flag){
                CustException.cust(AppHttpCodeEnum.ARTICLE_ADD_ERROR);
            }

        }

        //更新ApUser数据中 flag =1
        apUser.setFlag((short) 1);
        apUserMapper.updateById(apUser);
        //返回结果

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 创建作者账户
     * @param wmUser
     * @return
     */
    private boolean createApAuthor(WmUser wmUser) {
        //查询当前作者是否存在，不存在则创建
        ApAuthor apAuthor = articleFeign.findByUserId(wmUser.getApUserId());
        if (apAuthor == null){
            apAuthor = new ApAuthor();
            apAuthor.setCreatedTime(new Date());
            apAuthor.setUserId(wmUser.getApUserId());
            apAuthor.setWmUserId(wmUser.getId());
            apAuthor.setName(wmUser.getName());
            apAuthor.setType(AdminConstants.AUTHOR_TYPE);
            ResponseResult result = articleFeign.save(apAuthor);

            if (result.getCode() == 0){
                return true;
            }
        }
        return false;
    }

    /**
     * 创建自媒体账户
     * @param dto
     * @param apUser
     * @return
     */
    private WmUser createWmUser(AuthDto dto, ApUser apUser) {

        //查询自媒体账户是否存在
        WmUser wmUser = wemediaFeign.findByName(apUser.getName());

        if (wmUser == null){
            wmUser = new WmUser();
            wmUser.setCreatedTime(new Date());
            wmUser.setName(apUser.getName());
            wmUser.setImage(apUser.getImage());
            wmUser.setPassword(apUser.getPassword());
            wmUser.setPhone(apUser.getPhone());
            wmUser.setSalt(apUser.getSalt());
            wmUser.setType(0);
            wmUser.setScore(100);
            wmUser.setNickname(apUser.getName());
            wmUser.setApUserId(apUser.getId());
            wmUser.setStatus(AdminConstants.PASS_AUTH.intValue());

            wmUser = wemediaFeign.save(wmUser);


            return wmUser ;
        }

        return null;
    }

    /**
     * 根据状态查询用户认证列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult loadListByStatus(AuthDto dto) {
        //校验参数
        if (dto == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();
        //按状态分页查询

        IPage<ApUserRealname> page = new Page<>(dto.getPage(),dto.getSize());
        LambdaQueryWrapper<ApUserRealname> query = new LambdaQueryWrapper<>();


        if (dto.getStatus() != null){
            query.eq(ApUserRealname::getStatus,dto.getStatus());
        }

        IPage<ApUserRealname> pageResult = page(page, query);

        //返回结果

        return new PageResponseResult(dto.getPage(),dto.getSize(),pageResult.getTotal(),pageResult.getRecords());
    }
}
