package com.heima.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.feigns.article.ArticleFeign;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.behavior.dtos.FollowBehaviorDto;
import com.heima.model.common.constants.FollowBehaviorConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.UserRelationDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserFan;
import com.heima.model.user.pojos.ApUserFollow;
import com.heima.user.mapper.ApUserFanMapper;
import com.heima.user.mapper.ApUserFollowMapper;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserRelationService;
import com.heima.utils.threadlocal.AppThreadLocalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ApUserRelationServiceImpl implements ApUserRelationService {

    @Autowired
    ArticleFeign articleFeign;
    /**
     * 用户关注/取消关注
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult follow(UserRelationDto dto) {
        //参数校验
        if (dto.getOperation() == null || dto.getOperation() < 0 || dto.getOperation() > 1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //判断是否有作者
        if (dto.getAuthorId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"作者不存在");
        }
        ApAuthor apAuthor = articleFeign.selectById(dto.getAuthorId());
        if (apAuthor == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"当前关注用户不存在");
        }

        //判断是否登录
        ApUser user = AppThreadLocalUtils.getUser();
        if (user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //关注
        if (dto.getOperation().intValue() == 0){
            // 创建数据（app用户关注和app用户粉丝）
            return followByUserId(user.getId(),apAuthor.getUserId(),dto.getArticleId());

        }else {     //取关
            return followCancelByUserId(user.getId(),apAuthor.getUserId());
        }
    }

    @Autowired
    ApUserMapper apUserMapper;

    @Autowired
    ApUserFanMapper apUserFanMapper;

    @Autowired
    ApUserFollowMapper apUserFollowMapper;

    @Autowired
    KafkaTemplate kafkaTemplate;


    /**
     *  取关
     * @param userId     当前登录的人
     * @param followUserId     被关注的对应的apUser id
     * @return
     */
    private ResponseResult followCancelByUserId(Integer userId, Integer followUserId) {

        //删除我的关注
        apUserFollowMapper.delete(Wrappers.<ApUserFollow>lambdaQuery()
                .eq(ApUserFollow::getFollowId,followUserId)
                .eq(ApUserFollow::getUserId,userId)
        );

        //删除作者的粉丝
        apUserFanMapper.delete(Wrappers.<ApUserFan>lambdaQuery()
                .eq(ApUserFan::getFansId,userId)
                .eq(ApUserFan::getUserId,followUserId)
        );

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    /**
     *  关注
     * @param userId  当前用户的id
     * @param followUserId      当前用户关注的作者的user id
     * @param articleId     文章id
     * @return
     */
    private ResponseResult followByUserId(Integer userId, Integer followUserId, Long articleId) {
        //查询作者的用户id是否存在
        ApUser followUser = apUserMapper.selectById(followUserId);
        if (followUser.getId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"作者不存在");
        }
        //是否关注自己
        if (userId.intValue() == followUser.getId().intValue()){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"无需关注自己");
        }

        //保存我的关注  关注数据
        ApUserFollow apUserFollow = apUserFollowMapper.selectOne(Wrappers.<ApUserFollow>lambdaQuery()
                .eq(ApUserFollow::getFollowId, followUser.getId())
                .eq(ApUserFollow::getUserId, userId)
        );
        if (apUserFollow == null){
            apUserFollow = new ApUserFollow();
            apUserFollow.setFollowId(followUserId);
            apUserFollow.setUserId(userId);
            apUserFollow.setLevel((short)1);
            apUserFollow.setIsNotice(true);
            apUserFollow.setFollowName(followUser.getName());
            apUserFollow.setCreatedTime(new Date());
            apUserFollowMapper.insert(apUserFollow);
        }

        //保存作者对应的 粉丝数据
        ApUserFan apUserFan = apUserFanMapper.selectOne(Wrappers.<ApUserFan>lambdaQuery()
                .eq(ApUserFan::getFansId, userId)
                .eq(ApUserFan::getUserId, followUserId)
        );
        if (apUserFan == null){
            apUserFan = new ApUserFan();
            apUserFan.setUserId(followUserId);
            apUserFan.setCreatedTime(new Date());
            apUserFan.setLevel((short)0);
            apUserFan.setIsShieldComment(false);
            apUserFan.setIsShieldLetter(false);
            apUserFan.setIsDisplay(true);
            apUserFan.setFansId(userId.longValue());
            //粉丝的名称
            ApUser apUser = apUserMapper.selectById(userId);
            apUserFan.setFansName(apUser.getName());
            apUserFanMapper.insert(apUserFan);
        }


        //*****异步记录用户关注行为

        FollowBehaviorDto followBehaviorDto = new FollowBehaviorDto();
        followBehaviorDto.setArticleId(articleId);
        followBehaviorDto.setFollowId(followUserId);
        followBehaviorDto.setUserId(userId);
        kafkaTemplate.send(FollowBehaviorConstants.FOLLOW_BEHAVIOR_TOPIC, JSON.toJSONString(followBehaviorDto));

        //返回结果
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

}
