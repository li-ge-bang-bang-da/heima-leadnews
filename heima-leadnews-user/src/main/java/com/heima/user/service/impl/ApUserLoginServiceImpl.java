package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserLoginService;
import com.heima.utils.common.AppJwtUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.Map;

@Service
public class ApUserLoginServiceImpl implements ApUserLoginService {

    @Autowired
    ApUserMapper apUserMapper;
    /**
     * app端登录
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto dto) {
        //参数校验
        if (StringUtils.isBlank(dto.getPhone()) || StringUtils.isBlank(dto.getPassword())
                || dto.getEquipmentId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //判断用户登录状态
        //用户登录
        if (StringUtils.isNotBlank(dto.getPhone()) && StringUtils.isNotBlank(dto.getPassword())){
            ApUser user = apUserMapper.selectOne(Wrappers.<ApUser>lambdaQuery()
                    .eq(ApUser::getPhone,dto.getPhone())
                    .eq(ApUser::getStatus,0)
            );

            if (user == null){
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
            }

            // 获取数据库密码
            String dbpassword = user.getPassword();
            String newpassword = DigestUtils.md5DigestAsHex((dto.getPassword() + user.getSalt()).getBytes());
            //比对密码
            if (!newpassword.equals(dbpassword)){
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"手机号或密码错误");
            }

            //生成JWT令牌和用户信息返回前端
            Map<String, Object> map = new HashMap<>();
            map.put("token",AppJwtUtil.getToken(user.getId().longValue()));
            map.put("user",user);
            user.setPassword(null);
            user.setSalt(null);

            //返回结果
            return ResponseResult.okResult(map);

        }else {     //不登录先看看

            Map<String, Object> map = new HashMap<>();
//            map.put("token",AppJwtUtil.getToken(dto.getEquipmentId().longValue()));
            map.put("token",AppJwtUtil.getToken(0L));
            return ResponseResult.okResult(map);

        }
    }
}
