package com.heima.model.comment;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

import java.util.Date;

@Data
public class CommentRepayDto extends PageRequestDto {
    /**
     * 评论id
     */
    private String commentId;

    private Integer size;

    // 最小时间
    private Date minDate;
}
