package com.heima.model.common.constants;

public class HotArticleConstants {

    // 接收用户文章行为后发送消息topic
    public static final String HOTARTICLE_SCORE_INPUT_TOPIC="hot.article.score.topic";

    // 计算文章分值成功后发送消息topic
    public static final String HOTARTICLE_INCR_HANDLE_OUPUT_TOPIC="hot.article.incr.handle.topic";
}
