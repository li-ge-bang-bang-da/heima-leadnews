package com.heima.model.common.enums;

public enum AppHttpCodeEnum {

    // 成功段0
    SUCCESS(0,"操作成功"),
    // 登录段1~50
    NEED_LOGIN(1,"需要登录后操作"),
    LOGIN_PASSWORD_ERROR(2,"密码错误"),
    // TOKEN50~100
    TOKEN_INVALID(50,"无效的TOKEN"),
    TOKEN_EXPIRE(51,"TOKEN已过期"),
    TOKEN_REQUIRE(52,"TOKEN是必须的"),
    // SIGN验签 100~120
    SIGN_INVALID(100,"无效的SIGN"),
    SIG_TIMEOUT(101,"SIGN已过期"),
    // 参数错误 500~1000
    PARAM_REQUIRE(500,"缺少参数"),
    PARAM_INVALID(501,"无效参数"),
    PARAM_IMAGE_FORMAT_ERROR(502,"图片格式有误"),
    SERVER_ERROR(503,"服务器内部错误"),
    FILE_UPLOAD_ERROR(504,"上传失败"),
    IMAGE_USE_ERROR(505,"图片已被使用"),
    IMAGE_DELETE_ERROR(505,"图片删除失败"),
    // 数据错误 1000~2000
    DATA_EXIST(1000,"数据已经存在"),
    AP_USER_DATA_NOT_EXIST(1001,"ApUser数据不存在"),
    DATA_NOT_EXIST(1002,"数据不存在"),
    // 数据错误 3000~3500
    NO_OPERATOR_AUTH(3000,"无权限操作"),
    NEED_ADMIND(3001,"需要管理员权限"),

    // 数据错误 4000~4999
    CHANNEL_ISDEFAULT_INVALID(4000,"默认频道无法删除"),
    ADMIN_CHANNEL_ISSTATUS_ERROR(4001,"频道占用，无法删除"),
    ADMIN_ADUSER_ISNTOEXIT_ERROR(4002,"当前用户可能被锁定或不存在"),
    ADMIN_ADUSER_NAMEORPWD_ERROR(4003,"用户名或密码错误"),

    // 数据错误 5000~5999
    APUSER_STATUS_ERROR(5001,"当前用户不存在或被锁定"),
    WMUSER_ADD_ERROR(5004,"自媒体账户创建失败"),
    WEMEDIA_UPDATE_ERROR(5005,"自媒体文章修改失败"),
    ARTICLE_ADD_ERROR(5002,"作者账户创建失败"),
    FILE_MATERIAL_ISNULL_ERROR(5003,"素材列表不存在");

    int code;
    String errorMessage;

    AppHttpCodeEnum(int code, String errorMessage){
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public int getCode() {
        return code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
