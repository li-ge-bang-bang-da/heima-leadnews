package com.heima.model.common.constants;

public class ArticleConstants {

    public static final Short LOADTYPE_LOAD_MORE = 1;  // 加载更多  上拉
    public static final Short LOADTYPE_LOAD_NEW = 2; // 加载最新    下拉
    public static final String DEFAULT_TAG = "__all__";     //默认频道

    // 文章行为分值
    public static final Integer HOT_ARTICLE_VIEW_WEIGHT = 1;       //阅读
    public static final Integer HOT_ARTICLE_LIKE_WEIGHT = 3;        //点赞
    public static final Integer HOT_ARTICLE_COMMENT_WEIGHT = 5;         //评论
    public static final Integer HOT_ARTICLE_COLLECTION_WEIGHT = 8;      //收藏

    public static final Integer HOT_ARTICLE_SCORE = 3;      //实时计算文章分值的比重

    // 存到redis热文章前缀
    public static final String HOT_ARTICLE_FIRST_PAGE = "hot_article_first_page_";

    public static final String ARTICLE_ES_SYNC_TOPIC = "article.es.sync.topic";
}
