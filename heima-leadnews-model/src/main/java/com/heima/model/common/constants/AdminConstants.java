package com.heima.model.common.constants;

public class AdminConstants {

    public static final Integer ADUSER_NORMAL_STATUS = 9; //用户状态

    //审核状态
    public static final Short WAIT_AUTH = 1; //待审核
    public static final Short PASS_AUTH = 9; //审核通过
    public static final Short FAIL_AUTH = 2; //审核失败

    public static final Integer AUTHOR_TYPE = 2; // 自媒体用户
}
