package com.heima.model.article.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleHomeDto {

    // 最大时间  下拉刷新
    Date maxBehotTime;
    // 最小时间  上拉获取更多
    Date minBehotTime;
    // 分页size
    Integer size;
    // 频道ID
    String tag;
}
