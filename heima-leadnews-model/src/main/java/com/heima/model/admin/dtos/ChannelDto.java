package com.heima.model.admin.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("频道查询实体")
public class ChannelDto extends PageRequestDto {

    //频道名称
    @ApiModelProperty("频道名称")
    private String name;

    //频道状态，0以外都是启用
    @ApiModelProperty("频道状态")
    private Boolean status;

}
