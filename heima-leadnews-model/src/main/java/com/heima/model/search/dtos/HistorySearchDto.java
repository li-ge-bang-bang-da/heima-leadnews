package com.heima.model.search.dtos;

import lombok.Data;

@Data
public class HistorySearchDto {

    // 设备ID
    Integer equipmentId;
    /**
     * 接收搜索历史记录id
     */
    String historyId;
}
