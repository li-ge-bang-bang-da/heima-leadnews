package com.heima.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * 扫描feign对应的接口
 */
@Configuration
@EnableFeignClients("com.heima.feigns")
public class HeimaFeignAutoConfiguration {
}
