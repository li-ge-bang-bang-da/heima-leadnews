package com.heima.wemedia.gateway.filter;

import com.heima.wemedia.gateway.utils.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
@Order(1)
public class AuthorizeFilter implements GlobalFilter {

    /**
     * 全局过滤器
     * @param exchange
     * @param chain
     * @return
     */

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取request 和 response 对象,判断当前是不是登录请求，是的则放行
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String url = request.getURI().getPath();

        if (url.contains("/login/in")){
            return chain.filter(exchange);
        }

        //获取jwt token 信息
        String token = request.getHeaders().getFirst("token");
        if (StringUtils.isEmpty(token)){
            //如果token不存在，则向前端返回错误
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        try {
            //判断令牌信息是否正确
            Claims claims = AppJwtUtil.getClaimsBody(token);
            int verifyToken = AppJwtUtil.verifyToken(claims);
                //如果不存在或失效，则拦截
            if (verifyToken>0){
                log.error("token is overdue,url:{},claims:{}",url,claims);
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }

            //解析token
            Integer id = claims.get("id",Integer.class);

            //将令牌信息发送到各个微服务
            ServerHttpRequest httpRequest = request.mutate().headers(httpHeaders -> {
                httpHeaders.add("userId", id.toString());
            }).build();
            exchange.mutate().request(httpRequest).build();


        } catch (Exception e) {
//            e.printStackTrace();
            log.error("verifyToken is error,url:{}",url);
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //返回结果
        return chain.filter(exchange);
    }

}
